var map = null;
var geofences = [];
var shipObj = new Array();
var infowindow = new google.maps.InfoWindow({
	content : ""
});

function createNewPoint(x, y) {
	return new google.maps.LatLng(x, y);
}

$(function() {
	init();
	function init() {
		draw_map();
		init_ship_picker();
		draw_geofences();
		$(window).resize(resize_div);
		function resize_div() {
			var height_window = $(window).height();
			var height_menu_bar = $("#dofm-menu-bar").height();
			var div_height = height_window - height_menu_bar;
			$("#map-canvas").height(div_height);
			$("#container-menu").height(div_height);
		}
		function draw_map() {
			var myLatlng = new google.maps.LatLng(2.953327, 101.325188);
			var mapOptions = {
				zoom : 10,
				center : myLatlng
			};

			var map_element = document.getElementById('map-canvas');
			map = new google.maps.Map(map_element, mapOptions);
			resize_div();
		}
	}

	function init_ship_picker() {
		var url = $("#link-list-ship-name").attr("href");
		var settings = {
			success : function(data) {
				var n = data.length;
				var i = 0;
				var select = $("#input-template > .row > div select");
				for (i = 0; i < n; i++) {
					if (data[i]["name"] == "") {
						continue;
					}
					var opt = $("<option></option>");
					opt.attr("value", data[i]["id"]);
					opt.text(data[i]["name"]);
					select.append(opt);
				}
				add_ship_drop_down();
			}
		};
		$.ajax(url, settings);
	}
	
	function draw_geofences() {
		function create_geofence(geofence) {
			var data = geofence.points;
			var n = data.length;
			var i;
			var coordinates = [];
			var item = null;
			for (i = 0; i < n; i++) {
				var x = data[i][1];
				var y = data[i][0];
				item = createNewPoint(x, y);
				coordinates[i] = item;
			}
			if (geofence.closed == true) {
				coordinates[i] = coordinates[0];
			}
			var flightPath = new google.maps.Polyline({
				path : coordinates,
				geodesic : true,
				strokeColor : 'orange',
				strokeOpacity : 1.0,
				strokeWeight : 2
			});
			flightPath.setMap(map);
			return flightPath;
		}
		function create_geofences(data) {
			var lines = [];
			var n = data.length;
			var i;
			for (i = 0; i < n; i++) {
				var line = create_geofence(data[i]);
				lines.push(line);
			}
			geofences = lines;
		}
		var url = $("#link-geofences").attr("href");
		var settings = {
			error : function() {
			},
			success : function(data) {
				create_geofences(data);
			}
		};
		$.ajax(url, settings);

	}

});

function add_ship_drop_down() {
	var row = $("#input-template .row-select-ship-name").clone();
	$(".cpButton", row).colorpicker({'showOn' : 'button'});
	var ctx = "#container-menu > .row-select-ship-name:last";
	show_plus_minus(false, true, $(ctx));
	$("#row-date-from").before(row);
	if ($("#container-menu > .row-select-ship-name").length == 1) {
		show_plus_minus(true, false, $(ctx));
	} else {
		show_plus_minus(true, true, $(ctx));
	}
	
}

function show_plus_minus(show_plus, show_minus, ctx) {
	if (ctx.length == 0) {
		return;
	}
	var css_plus = css_display(show_plus);
	var css_minus = css_display(show_minus);
	$(".fa-plus", ctx).css(css_plus);
	$(".fa-minus", ctx).css(css_minus);
	var show_blank = false;
	if (show_plus == true && show_minus == true) {
		show_blank = true;
	}
	$(".dofm-playback-add-remove-spacer", ctx).css(css_display(show_blank));

	function css_display(show) {
		var obj = {
			"display" : "none",
		};
		if (show) {
			obj["display"] = "";

		}
		return obj;
	}
}

$(function() {
	var sel = ".row-select-ship-name .action-insert-ship-row";
	$(document).on("click", sel, function(event) {
		event.stopPropagation();
		add_ship_drop_down();
		return false;
	});
});

$(function() {
	var sel = ".row-select-ship-name .action-remove-ship-row";
	$(document).on("click", sel, function(event) {
		event.stopPropagation();
		remove_ship_drop_down(event);
		return false;
	});
	function remove_ship_drop_down(e) {
		var elem = e.currentTarget;
		$(elem).parents(".row-select-ship-name").remove();
		var ctx = "#container-menu > .row-select-ship-name:last";
		if ($("#container-menu > .row-select-ship-name").length > 1) {
			show_plus_minus(true, true, $(ctx));
		} else {
			show_plus_minus(true, false, $(ctx));
		}

	}
});

$(function() {
	$("#from-date, #to-date").datepicker({
		changeMonth : true,
		changeYear : true,
		dateFormat : "dd/mm/yy"
	});
});

$(function() {
	var ships;
	var items;
	//var polyColor = ['#000000', '#FF0000'];
	var shipPoly = [];
	var shipPath = [];
	var timeouts = [];
	
	$("#button-plot").click(
			function() {
				var dateTimeFrom = create_date_time($("#from-date"),
						$("#from-hour"), $("#from-minute"), $("#from-am-pm"));
				$("#from").val(dateTimeFrom);
				var dateTimeTo = create_date_time($("#to-date"), $("#to-hour"),
						$("#to-minute"), $("#to-am-pm"));
				$("#to").val(dateTimeTo);
				var options = {
					"success" : function(data) {
						draw_ship_path(data);
					}
				};
				$("#form-plot").ajaxSubmit(options);
				return false;
			});
	
	$('#button-clearplot').click(function(){
		for (var i = 0; i < timeouts.length; i++) {
		    clearTimeout(timeouts[i]);
		}
		timeouts = [];
		$.each(ships,function(ship){
			shipPoly[ship].setMap(null);
			shipObj[ship]['start']['marker'].setMap(null);
			if(shipObj[ship]['end']['marker'] != undefined){
				shipObj[ship]['end']['marker'].setMap(null);
			}
		});
		shipPoly = [];
		shipPath = [];
		shipObj = new Array();
		
	});
	
	$("#checkbox-geofence").click(function() {
		var $this = $(this);
		var draw = $this.is(':checked');
		$(this).prop("disabled", true);
		if (geofences.length > 0) {
			var n = geofences.length;
			var i;
			for (i = 0; i < n; i++) {
				geofences[i].setVisible(draw);
			}
		} else {
			if (draw) {
				draw_geofences();
			}
		}
		$(this).prop("disabled", false);
	});
	
	function draw_ship_path(data){
		ships = data.ship;
		items = data.items; 
		$.each(ships, function(ship,value){
			shipObj[ship] = new Array();
		  	shipObj[ship]['path'] = new Array();
		  	shipObj[ship]['path_info'] = new Array();
		  	shipObj[ship]['start'] = new Array();
		  	shipObj[ship]['end'] = new Array();
		  	shipObj[ship]['name'] = value;
		});
		
		$.each($('#form-plot select[name="mmsi"]'),function(){ 
			var shipcode = $(this).val();
			shipObj[shipcode]['color'] = $(this).parent().find('.cpButton').val();	
		});
		
		for ( var i = 0; i < items.length; i++) {
			var time = items[i].time;
			var timestamp = items[i].timestamp;
			var path_info = new Array();
			path_info["time"] = time;
			path_info["timestamp"] = timestamp;
			shipObj[items[i].mmsi]['path'].push(createNewPoint(items[i].point[1],items[i].point[0]));
			shipObj[items[i].mmsi]['path_info'].push(path_info);
		}
		
		$.each(ships, function(ship,data){ 
			for ( var i = 0; i < shipObj[ship]['path'].length; i++) {
				if(i == 0){
					shipObj[ship]['start']['timestamp'] = shipObj[ship]['path_info'][i]['timestamp'];
					shipObj[ship]['start']['time'] = shipObj[ship]['path_info'][i]['time'];
				}
				if(i == shipObj[ship]['path'].length - 1){
					shipObj[ship]['end']['timestamp'] = shipObj[ship]['path_info'][i]['timestamp'];
					shipObj[ship]['end']['time'] = shipObj[ship]['path_info'][i]['time'];
				}
			}
		});
		
		var timer = $('#delay').val();

		 $.each(ships, function(ship){
			 shipPoly[ship] = new google.maps.Polyline({
				 map: map,
		  	     geodesic : true,
		  	     strokeColor : shipObj[ship]['color'],
		  	     strokeOpacity : 1.0,
		  	     strokeWeight : 2
		  	 });
			 shipPath[ship] = new google.maps.MVCArray();
		 });
		    
		 $.each(ships, function(ship){ 
			 for ( var i = 0; i < shipObj[ship]['path'].length; i++) {
				 if(i === 0) {
					 shipPath[ship].push(shipObj[ship]['path'][i]);
					 shipPoly[ship].setPath(shipPath[ship]);
					 shipObj[ship]['start']['marker'] = create_marker(ship,"start");
		  	     } else { 
		  	    	timeouts.push(setTimeout((function(latLng,ship,i) { 
		  		        return function() {
		  		        	shipPath[ship].push(latLng); 
		  		        	if(i == shipObj[ship]['path'].length - 1){ 
		  		        		shipObj[ship]['end']['marker'] = create_marker(ship,"end");
		  		        		
		  		        	}
		  		        };
		  		      })(shipObj[ship]['path'][i],ship,i), timer * i)
		  	    	);
		  	     }
			 }
			 open_info_window(ship);
		  });
		 
	}
	
	function create_marker(ship,type){
		var position;
		
		if(type == "start")
			position = shipObj[ship]['path'][0];
		else 
			position = shipObj[ship]['path'][shipObj[ship]['path'].length-1];
		
		return new google.maps.Marker({
		 	icon: create_ship_icon(ship,type),
      	    position: position,
      	    title: create_ship_title(ship,type),
      	    map: map
      	  });
	}
	
	function create_ship_icon(ship, type) {
		var colour = shipObj[ship]['color'];
		var icon = google.maps.SymbolPath.FORWARD_CLOSED_ARROW;
		
		if(type == "end")
			icon = 'M0,0 2,0 2,2 0,2 Z';
		
		var symbol = {
			path : icon,
			scale : 3,
			fillColor : colour,
			strokeColor : colour,
			fillOpacity : 1,
			rotation : 0
		};
		return symbol;

	}
	
	function create_ship_title(ship,type) {
		var title = "";
		if(type == "start"){
			title = title + "Start ( " + shipObj[ship]['start']['timestamp'] + " )";
		} else {
			title = title + "End ( " + shipObj[ship]['end']['timestamp'] + " )";
		}
		title = title + "\n";
		title = title + "Name: " + shipObj[ship]['name'];
		return title;
	}
	
	function open_info_window(ship) {
		var poly = shipPoly[ship];
		
		google.maps.event.addListener(poly, 'click', function(event) { 
			   var marker = new google.maps.Marker({
			      position: event.latLng
			   });
			   var content_text = "Ship : " + shipObj[ship]['name'] + "<br/>Start : " + shipObj[ship]['start']['timestamp'] + "<br/>End : " + shipObj[ship]['end']['timestamp'];
			   var content = "<div id='google-map-info-window' style='width:250px;margin:1px 1px 1px 1px;'>" + 
			   		content_text + "</div>";
			   infowindow.setContent(content);
			   infowindow.open(map, marker);
	
		});
	}
	
	function create_date_time(elemDate, elemHour, elemMinute, elemAmPm) {
		var valDate = elemDate.val();
		var valHour = elemHour.val();
		var valMinute = elemMinute.val();
		var valSeconds = "00";
		var valAmPm = elemAmPm.val();
		var val = "";
		val = val + valDate;
		val = val + " ";
		val = val + valHour + ":" + valMinute + ":" + valSeconds;
		val = val + " ";
		val = val + valAmPm;
		return val;
	}
});