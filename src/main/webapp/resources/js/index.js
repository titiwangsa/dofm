$(function() {

	var map = null;
	var ship_data_map = {};
	var shorelines = [];
	var geofences = [];
	var list_insert = [];
	var list_update = [];
	var list_delete = [];
	var server_time_in_ms = 0;
	var ship_line = null;
	var open_infowindow_mmsi = 0;
	var all_vessels_visible = false;

	var infowindow = new google.maps.InfoWindow({
		content : ""
	});

	init();
	draw_map();
	draw_geofences();
	draw_ships();

	function init() {
		init_map_canvas_height();

		function init_map_canvas_height() {
			var hBody = $(window).height();
			var hBreadcrumb = $("#container-breadcrumb").height();
			$("#map-canvas-container, #map-canvas").height(hBody - hBreadcrumb);
		}

	}

	function createNewPoint(x, y) {
		return new google.maps.LatLng(x, y);
	}

	function get_key_from_object(object) {
		var list = [];
		var key = null;
		for (key in object) {
			list.push(key);
		}
		return list;
	}

	function draw_map() {
		var myLatlng = createNewPoint(2.953327, 101.325188);
		var mapOptions = {
			zoom : 10,
			center : myLatlng
		};

		var map_element = document.getElementById('map-canvas');
		map = new google.maps.Map(map_element, mapOptions);
	}

	function draw_shorelines() {
		function create_shoreline(data) {
			var n = data.length;
			var i;
			var coordinates = [];
			var item = null;
			for (i = 0; i < n; i++) {
				var x = data[i].x;
				var y = data[i].y;
				item = createNewPoint(x, y);
				coordinates[i] = item;
			}
			var flightPath = new google.maps.Polyline({
				path : coordinates,
				geodesic : true,
				strokeColor : '#FF0000',
				strokeOpacity : 1.0,
				strokeWeight : 2
			});
			flightPath.setMap(map);
			shorelines.push(flightPath);
		}
		function create_shorelines(data) {
			var n = data.length;
			var i;
			for (i = 0; i < n; i++) {
				create_shoreline(data[i]);
			}
		}
		var url = $("#link-shorelines").attr("href");
		var settings = {
			error : function() {
			},
			success : function(data) {
				create_shorelines(data);
			}
		};
		$.ajax(url, settings);
	}

	function draw_geofences() {
		function create_geofence(geofence) {
			var data = geofence.points;
			var n = data.length;
			var i;
			var coordinates = [];
			var item = null;
			for (i = 0; i < n; i++) {
				var x = data[i][1];
				var y = data[i][0];
				item = createNewPoint(x, y);
				coordinates[i] = item;
			}
			if (geofence.closed == true) {
				coordinates[i] = coordinates[0];
			}
			var flightPath = new google.maps.Polyline({
				path : coordinates,
				geodesic : true,
				strokeColor : 'orange',
				strokeOpacity : 1.0,
				strokeWeight : 2
			});
			flightPath.setMap(map);
			return flightPath;
		}
		function create_geofences(data) {
			var lines = [];
			var n = data.length;
			var i;
			for (i = 0; i < n; i++) {
				var line = create_geofence(data[i]);
				lines.push(line);
			}
			geofences = lines;
		}
		var url = $("#link-geofences").attr("href");
		var settings = {
			error : function() {
			},
			success : function(data) {
				create_geofences(data);
			}
		};
		$.ajax(url, settings);

	}

	function draw_ships_defer() {
		draw_ships();
	}

	function draw_ships() {
		var url = $("#link-coordinates").attr("href");
		var settings = {
			cache : false,
			error : function() {
			},
			success : function(responseData) {
				server_time_in_ms = responseData.timestamp;
				var data = responseData.items;
				var data_map = create_data_map(data);
				put_in_category(data_map);
				process_ships(data_map);
				ship_data_map = data_map;
				setTimeout(draw_ships_defer, 1000 * 10);
			}
		};

		function process_ships(data_map) {
			process_update_ships(data_map);
			process_insert_ships(data_map);
			process_delete_ships();
		}

		function update_ship_on_map(ship_data) {
			var index = ship_data["mmsi"];
			var map_data = ship_data_map[index]["data"];
			if (map_data["lastModifiedDate"] != ship_data["lastModifiedDate"]) {
				return true;
			} else {
				return false;
			}
		}

		function process_update_ships(data_map) {
			var n = list_update.length;
			var i = 0;
			for (i = 0; i < n; i++) {
				var key = list_update[i];
				data_map[key]["marker"] = ship_data_map[key]["marker"];
				var updated = update_ship_on_map(data_map[key]["data"]);
				if (updated) {
					populate_violations(data_map, key);
					var ship_data = data_map[key]["data"];
					var marker = data_map[key]["marker"];
					var itemPoint = ship_data.point;
					var itemLat = itemPoint.x;
					var itemLng = itemPoint.y;
					var pos = createNewPoint(itemLat, itemLng);
					var icon = create_ship_icon(data_map, key);
					var title = create_ship_title(data_map, key);
					marker.setPosition(pos);
					marker.setIcon(icon);
					marker.setTitle(title);
					handle_ship_click(data_map, key);
					if (open_infowindow_mmsi == key) {
						var html = create_content_text(title);
						$("#google-map-info-window").html(html);
					}
				}
			}
		}

		function list_violations_if_any(activePosition) {
			var list = [];
			var lastUpdated = activePosition.lastModifiedTimeStamp;
			if (activePosition.sog >= 1.0 && activePosition.sog <= 4.0) {
				var d = Math.round(activePosition.distanceToShore * 100);
				var distanceToShore = parseFloat(d / 100).toFixed(2);
				if (distanceToShore >= 1.0 && distanceToShore <= 5.0) {
					list.push("Geofence Violation");
				}
			}
			var time_dif = server_time_in_ms - lastUpdated;
			if (time_dif >= (12 * 60 * 60 * 1000)) {
				if (time_dif >= (24 * 60 * 60 * 1000)) {
					list.push("24hr No Update");
				} else {
					list.push("12hr No Update");
				}
			}
			return list;
		}

		function create_ship_icon(data_map, key) {
			var item = data_map[key]["data"];
			var degree = item.degree;
			var colour;
			if (item["fishingFleet"] == false) {
				colour = "green";
			} else {
				if (data_map[key]["violations"].length == 0) {
					colour = "blue";
				} else {
					colour = "red";
				}
			}
			var icon = google.maps.SymbolPath.FORWARD_CLOSED_ARROW;
			var rotation = degree;
			if (degree == 511) {
				if (item.cog != 360) {
					rotation = item.cog;
				}
			}
			if (item.sog == 0) {
				icon = 'M0,0 2,0 2,2 0,2 Z';
				rotation = 0;
			}
			var symbol = {
				path : icon,
				scale : 3,
				fillColor : colour,
				strokeColor : colour,
				fillOpacity : 1,
				rotation : rotation
			};
			return symbol;

		}

		function create_ship_title(data_map, key) {
			var item = data_map[key]["data"];
			var degree_label = "";
			if (item.sog > 0) {
				if (item.degree != 511 && item.degree < 360 && item.degree >= 0) {
					degree_label = "Heading: " + item.degree + "\xB0";
				} else {
					degree_label = "COG: " + item.cog + "\xB0";
				}
			}
			var title = "";
			title = title + "Name: " + item.name;
			title = title + "\n";
			title = title + "Speed: " + item.sog + " knots";
			title = title + "\n";
			if (degree_label != "") {
				title = title + degree_label;
				title = title + "\n";
			}
			if (item["fishingFleet"] == true) {
				var d = parseFloat(Math.round(item.distanceToShore * 100) / 100);
				var distanceToShore = d.toFixed(2);
				title = title + "Distance to shore: " + distanceToShore + "nm";
				title = title + "\n";
			}
			{
				var violations = data_map[key]["violations"];
				var n = violations.length;
				var i = 0;
				for (i = 0; i < n; i++) {
					title = title + "* " + violations[i];
					title = title + "\n";
				}
			}

			title = title + "Last Updated: " + item.lastModifiedDate;
			return title;
		}

		function create_marker_options(data_map, key) {
			var ship_data = data_map[key]["data"];
			var point = ship_data["point"];
			var options = {};
			var show = all_vessels_visible || ship_data["fishingFleet"];
			options["icon"] = create_ship_icon(data_map, key);
			options["map"] = map;
			options["draggable"] = false;
			options["position"] = createNewPoint(point.x, point.y);
			options["title"] = create_ship_title(data_map, key);
			options["visible"] = show;
			return options;
		}
		function populate_violations(data_map, key) {
			var ship_data = data_map[key]["data"];
			if (ship_data["fishingFleet"] == true) {
				data_map[key]["violations"] = list_violations_if_any(ship_data);
			} else {
				data_map[key]["violations"] = [];
			}
		}
		function insert_ship_on_map(data_map, key) {
			var markerOptions = create_marker_options(data_map, key);
			var marker = new google.maps.Marker(markerOptions);
			data_map[key]["marker"] = marker;
			handle_ship_click(data_map, key);
		}

		function create_content_text(title) {
			var find = '\n';
			var re = new RegExp(find, 'g');
			var content = title.replace(re, '<br/>');
			return content;

		}
		function open_info_window(data_map, key) {
			var marker = data_map[key]["marker"];
			var title = marker.getTitle();
			var content_text = create_content_text(title);
			var lines = content_text.split("<br/>").length;
			var content = "<div id='google-map-info-window' style='width:250px;margin:1px 1px 1px 1px;height:"
					+ (lines * 20) + "px;'>" + content_text + "</div>";
			infowindow.setContent(content);
			infowindow.open(map, marker);
		}
		function handle_ship_click(data_map, key) {
			if (ship_data_map[key] && ship_data_map[key]["click"]) {
				google.maps.event.removeListener(ship_data_map[key]["click"]);
			}
			var item = data_map[key]["data"];
			var click_event = function(event) {
				event.stop();
				remove_line();
				if (item["fishingFleet"] == true) {
					var coordinates = [];
					var from = item.point;
					var to = item.pointToShore;
					coordinates[0] = createNewPoint(from.x, from.y);
					coordinates[1] = createNewPoint(to.x, to.y);

					ship_line = new google.maps.Polyline({
						path : coordinates,
						geodesic : true,
						strokeColor : '#0000FF',
						strokeOpacity : 1.0,
						strokeWeight : 2
					});
					ship_line.setMap(map);
				}

				open_infowindow_mmsi = item.mmsi;
				open_info_window(data_map, key);
			};
			var markerItem = data_map[key]["marker"];
			var handle = google.maps.event.addListener(markerItem, 'click',
					click_event);
			data_map[key]["click"] = handle;

		}

		function process_insert_ships(data_map) {
			var n = list_insert.length;
			var i = 0;
			for (i = 0; i < n; i++) {
				var key = list_insert[i];
				populate_violations(data_map, key);
				insert_ship_on_map(data_map, key);
			}
		}

		function delete_ship_on_map(key) {
			ship_data_map[key]["marker"].setMap(null);
			ship_data_map[key]["marker"] = null;
			delete ship_data_map[key]["marker"];
			ship_data_map[key] = null;
			delete ship_data_map[key];
		}

		function process_delete_ships() {
			var n = list_delete.length;
			var i = 0;
			for (i = 0; i < n; i++) {
				var key = list_delete[i];
				delete_ship_on_map(key);
			}
		}

		function put_in_category(data_map) {
			var keys_current = get_key_from_object(data_map);
			var keys_on_map = get_key_from_object(ship_data_map);
			var keys_update = [];
			var i = 0;
			for (i = 0; i < keys_current.length; i++) {
				var key_current = keys_current[i];
				if (keys_on_map.indexOf(key_current) >= 0) {
					keys_update.push(key_current);
				}

			}
			keys_current = remove_key_from_object(keys_current, keys_update);
			keys_on_map = remove_key_from_object(keys_on_map, keys_update);
			list_insert = keys_current;
			list_update = keys_update;
			list_delete = keys_on_map;
		}

		function remove_key_from_object(list_keys, list_keys_to_remove) {
			var list = [];
			var i = 0;
			var n = list_keys.length;
			for (i = 0; i < n; i++) {
				var key_to_add = list_keys[i];
				if (list_keys_to_remove.indexOf(key_to_add) < 0) {
					list.push(key_to_add);
				}
			}
			return list;
		}

		function create_data_map(data) {
			var items = {};
			var i;
			var n = data.length;
			for (i = 0; i < n; i++) {
				var dataItem = data[i];
				var key = "" + dataItem.mmsi + "";
				items[key] = {};
				items[key]["data"] = dataItem;
			}
			return items;
		}

		$.ajax(url, settings);
		function remove_line() {
			if (ship_line != null) {
				ship_line.setMap(null);
				ship_line = null;
			}
		}
		function close_window() {
			infowindow.close();
			open_infowindow_mmsi = 0;
		}
		google.maps.event.addListener(map, 'click', remove_line);
		google.maps.event.addListener(map, 'click', close_window);
		google.maps.event.addListener(infowindow, 'closeclick', function() {
			open_infowindow_mmsi = 0;
			remove_line();
		});
	}

	// TODO apply when we click ok
	$("#checkbox-geofence").click(function() {
		var $this = $(this);
		var draw = $this.is(':checked');
		$(this).prop("disabled", true);
		if (geofences.length > 0) {
			var n = geofences.length;
			var i;
			for (i = 0; i < n; i++) {
				geofences[i].setVisible(draw);
			}
		} else {
			if (draw) {
				draw_geofences();
			}
		}
		$(this).prop("disabled", false);
	});

	// TODO apply when we click ok
	$("#checkbox-shoreline").click(function() {
		var $this = $(this);
		var draw = $this.is(':checked');
		$(this).prop("disabled", true);
		if (shorelines.length > 0) {
			var n = shorelines.length;
			var i;
			for (i = 0; i < n; i++) {
				shorelines[i].setVisible(draw);
			}
		} else {
			if (draw) {
				draw_shorelines();
			}
		}
		$(this).prop("disabled", false);
	});

	// TODO: apply when we click ok
	$("#checkbox-all-vessel").click(function() {
		var $this = $(this);
		all_vessels_visible = $this.is(':checked');
		$(this).prop("disabled", true);
		update_vessel_visibility_on_map();
		$(this).prop("disabled", false);
	});

	function update_vessel_visibility_on_map() {
		var keys_on_map = get_key_from_object(ship_data_map);
		var i = 0;
		var n = keys_on_map.length;
		for (i = 0; i < n; i++) {
			var key_current = keys_on_map[i];
			var item = ship_data_map[key_current];
			if (item["data"]["fishingFleet"] == false) {
				item["marker"].setVisible(all_vessels_visible);
			}
		}
	}

	$("#link-settings-done").click(function() {
		show_settings_view(false);
	});
	$("#link-settings").click(function() {
		// show_settings_view(true);
		$("#section-settings").dialog("open");
		return false;
	});

	$("#section-settings").dialog({
		"resizable" : false,
		"autoOpen" : false,
		"closeOnEscape" : false
	});

	function show_settings_view(show) {
		if (show) {
			$("#section-settings").removeClass("hidden-xs");
			$("#section-map").addClass("hidden-xs");
			$("#container-link-settings").attr("class", "hidden");
		} else {
			$("#section-map").removeClass("hidden-xs");
			$("#section-settings").addClass("hidden-xs");
			$("#container-link-settings").attr("class", "");
		}

	}

});