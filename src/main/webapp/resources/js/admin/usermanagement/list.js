$(function() {

	$("#dialog-change-password").dialog({
		"resizable" : false,
		"autoOpen" : false,
		"modal" : true,
		"buttons" : {
			"No" : function() {
				$(this).dialog("close");
			},
			"Update" : function() {
				if (execute_change_password()) {
					$(this).dialog("close");
				}
			}
		}
	});

	$("table").on("click", "a.link-change-password", function() {
		var tr = $(this).parents("tr");
		var username = $(".id", tr).text();
		var name = $(".name", tr).text();
		$("#form-change-password")[0].reset();
		$("#form-change-password-username").val(username);
		$("#form-change-password-name").val(name);
		$("#dialog-change-password").dialog("open");
		return false;
	});

	function execute_change_password() {
		var options = {
			"dataType" : "json",
			"success" : function(object, statusText, xhr, $form) {
				if (object.code != "0") {
					alert("Unable to change password for [" + object.username
							+ "].\n" + object.message);
				}
			}
		};
		if (isValidFormChangePassword()) {
			$("#form-change-password").ajaxSubmit(options);
			return true;
		} else {
			return false;
		}
	}

	function isValidFormChangePassword() {
		var messages = [];
		if ($("#form-change-password-password").val() == "") {
			messages.push("Password is mandatory.")
		}
		if ($("#form-change-password-password-confirm").val() == "") {
			messages.push("Confirm Password is mandatory.")
		}
		if ($("#form-change-password-password").val() != $(
				"#form-change-password-password-confirm").val()) {
			messages.push("Password confirmation mismatch.")
		}
		if (messages.length == 0) {
			return true;
		} else {
			alert(messages.join("\n"));
			return false;
		}
	}
})