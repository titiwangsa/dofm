$(function() {
	$("#form").submit(function() {
		var messages = [];
		if ($("#username").val() == "") {
			messages.push("Username is mandatory.")
		}
		if ($("#name").val() == "") {
			messages.push("Name is mandatory.")
		}
		if ($("#password").val() == "") {
			messages.push("Password is mandatory.")
		}
		if ($("#confirmPassword").val() == "") {
			messages.push("Confirm Password is mandatory.")
		}
		if ($("#confirmPassword").val() != $("#password").val()) {
			messages.push("Password confirmation mismatch.")
		}
		if (messages.length == 0) {
			return true;
		} else {
			alert(messages.join("\n"));
			return false;
		}

	});

});