$(function() {

	$("#button-add").click(function() {
		var areaName = $("#area-name").val();
		if (areaName.trim() != "") {
			if ($("#list-area option[value='" + areaName + "']").length == 0) {
				var list = $("#form-area-list select");
				var option = $("#template-select-option option").clone();
				option.val(areaName);
				option.text(areaName);
				option.attr("selected", "selected");
				list.append(option);
				$("#area-name").val("");
				execute_update();
			}
		}
		return false;
	});

	$("#button-delete").click(function() {
		var items = $("#list-area option:selected");
		if (items.length > 0) {
			var list = $("#form-area-list select");
			if (confirm('Confirm delete?')) {
				items.each(function() {
					$(this).remove();
					var val = $(this).val();
					$("option[value='" + val + "']", list).remove();
				});
			}
			execute_update();
		}
		return false;
	});

	function execute_update() {
		var options = {
			"success" : handle_response
		};
		function handle_response(response, statusText, xhr, $form) {
			var sel = $("#list-area");
			sel.children().remove();
			$("#list-area option").removeAttr("selected");
			var n = response.length;
			var i;
			var list = $("#list-area");
			for (i = 0; i < n; i++) {
				var option = $("<option></option>");
				option.val(response[i]);
				option.text(response[i]);
				list.append(option);
			}
		}
		$("#form-area").ajaxSubmit(options);
	}

})