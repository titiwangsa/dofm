/**
 * parent select must have (a) an id, (b) this class :
 * parent-child-select-parent
 * 
 * child select must have (a) class "parent-child-select-child-{parent id}", (b)
 * option with class "parent-child-select-option-template"
 * 
 * 
 * 
 */
$(function() {
	var pcs = "parent-child-select";
	$("select." + pcs + "-parent").change(function() {
		var parent_id = $(this).attr("id");
		var child = $("select." + pcs + "-child-" + parent_id)
		if (child.length == 0) {
			return;
		}
		var f = $("#form-" + pcs + "-" + parent_id)
		$("input.parameter-key", f).val($(this).val());
		submit_form(f, child);
	});

	function submit_form(f, element) {
		var success = function(data) {
			$("option." + pcs + "-option-added", element).remove();
			if (data.length == 0) {
				return;
			}
			if ("string" == typeof (data[0])) {
				populate_select_list(element, data);
			}
		}
		var options = {
			"success" : success
		};
		f.ajaxSubmit(options);
	}

	function populate_select_list(element, data) {
		var n = data.length;
		var i;
		var template_option = $("option." + pcs + "-option-template", element);
		var fnCloneTemplate = function() {
			return template_option.clone();
		};
		var fnBareOption = function() {
			return $("<option></option>");
		};
		var fnCreateOption = fnBareOption;
		if (template_option.length == 1) {
			fnCreateOption = fnCloneTemplate;
		}
		for (i = 0; i < n; i++) {
			var item = fnCreateOption();
			item.text(data[i])
			item.val(data[i]);
			item.addClass(pcs + "-option-added");
			element.append(item);
		}
		element.change();
	}

});