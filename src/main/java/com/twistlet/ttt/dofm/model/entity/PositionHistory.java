package com.twistlet.ttt.dofm.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class PositionHistory extends BasePosition {

	@Override
	public void setAis(final String ais) {
		super.setAis(ais);
		setId(ais);
	}

}
