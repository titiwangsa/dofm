package com.twistlet.ttt.dofm.model.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.Position;
import com.twistlet.ttt.dofm.model.entity.Vessel;
import com.twistlet.ttt.dofm.model.repository.PositionRepository;
import com.twistlet.ttt.dofm.model.repository.VesselRepository;

@Service
public class ListServiceImpl implements ListService {

	private final VesselRepository vesselRepository;
	private final PositionRepository positionRepository;

	@Autowired
	public ListServiceImpl(final VesselRepository vesselRepository,
			final PositionRepository positionRepository) {
		this.vesselRepository = vesselRepository;
		this.positionRepository = positionRepository;
	}

	@Override
	public List<IdPositionVessel> listPositionVessel() {
		final List<Position> listPosition = positionRepository.findAll();
		final List<Vessel> listVessel = vesselRepository.findAll();
		final Map<Integer, Position> mapPosition = toPositionMap(listPosition);
		final Map<Integer, Vessel> mapVessel = toVesselMap(listVessel);
		final List<Integer> listMmsi = new ArrayList<>(mapPosition.keySet());
		listMmsi.retainAll(mapVessel.keySet());
		final List<IdPositionVessel> list = new ArrayList<>();
		for (final Integer id : listMmsi) {
			final IdPositionVessel item = new IdPositionVessel();
			item.setId(id);
			item.setPosition(mapPosition.get(id));
			item.setVessel(mapVessel.get(id));
			list.add(item);
		}
		return list;
	}

	private Map<Integer, Vessel> toVesselMap(final List<Vessel> list) {
		final Map<Integer, Vessel> map = new LinkedHashMap<>();
		for (final Vessel vessel : list) {
			final Integer id = vessel.getMmsi();
			map.put(id, vessel);
		}
		return map;
	}

	private Map<Integer, Position> toPositionMap(final List<Position> list) {
		final Map<Integer, Position> map = new LinkedHashMap<>();
		for (final Position position : list) {
			final Integer id = position.getMmsi();
			map.put(id, position);
		}
		return map;
	}

}
