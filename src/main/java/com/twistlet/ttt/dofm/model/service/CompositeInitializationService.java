package com.twistlet.ttt.dofm.model.service;

import java.util.List;

public class CompositeInitializationService implements InitializationService {

	private List<InitializationService> list;

	public CompositeInitializationService(List<InitializationService> list) {
		super();
		this.list = list;
	}

	@Override
	public void initialize() {
		for (InitializationService service : list) {
			service.initialize();
		}
	}

}
