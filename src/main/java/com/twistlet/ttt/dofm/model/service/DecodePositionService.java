package com.twistlet.ttt.dofm.model.service;

import org.freeais.ais.AISPositionA;

public interface DecodePositionService {

	AISPositionA decode(String section);
}
