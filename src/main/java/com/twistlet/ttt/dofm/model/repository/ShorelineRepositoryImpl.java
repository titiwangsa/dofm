package com.twistlet.ttt.dofm.model.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.CustomMetric;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Metric;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.NearQuery;

import com.twistlet.ttt.dofm.model.entity.Shoreline;

public class ShorelineRepositoryImpl implements ShorelineRepositoryCustom {

	private final MongoOperations mongoOperations;

	@Autowired
	public ShorelineRepositoryImpl(final MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	@Override
	public GeoResults<Shoreline> findGeoResultsByPointNear(final double lon,
			final double lat) {
		final Metric nauticalMileMetric = new CustomMetric(3_443.89849);
		final NearQuery query = NearQuery.near(new Point(lon, lat));
		query.num(1);
		query.spherical(true);
		query.in(nauticalMileMetric);
		return mongoOperations.geoNear(query, Shoreline.class);
	}
}
