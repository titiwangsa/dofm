package com.twistlet.ttt.dofm.model.aggregator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.IntegrationMessageHeaderAccessor;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component("aisAggregatingMessageGroupProcessor")
public class AisAggregatingMessageGroupProcessor {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	public String aggregatePayloads(final List<Message<String>> items) {
		logger.debug("Received {} items", items.size());
		Collections.sort(items, new MessageSequenceComparator());
		final StringBuffer sb = new StringBuffer();
		for (final Message<String> message : items) {
			logMessageHeaders(message);
			final String payload = message.getPayload();
			sb.append(payload.split(",")[5]);
		}
		final String result = sb.toString();
		return result;
	}

	private void logMessageHeaders(final Message<String> message) {
		final IntegrationMessageHeaderAccessor accessor = new IntegrationMessageHeaderAccessor(
				message);
		final String payload = message.getPayload();
		final Object correlationId = accessor.getCorrelationId();
		final Integer sequenceNumber = accessor.getSequenceNumber();
		final Integer sequenceSize = accessor.getSequenceSize();
		final Object[] args = { correlationId, sequenceNumber, sequenceSize,
				payload };
		logger.debug("{} {}/{} {}", args);
	}

	private class MessageSequenceComparator implements
			Comparator<Message<String>> {

		@Override
		public int compare(final Message<String> o1, final Message<String> o2) {
			final IntegrationMessageHeaderAccessor accessor1 = new IntegrationMessageHeaderAccessor(
					o1);
			final IntegrationMessageHeaderAccessor accessor2 = new IntegrationMessageHeaderAccessor(
					o2);
			final Integer s1 = accessor1.getSequenceNumber();
			final Integer s2 = accessor2.getSequenceNumber();
			return (s1.compareTo(s2));
		}
	}
}
