package com.twistlet.ttt.dofm.model.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Vessel {

	@Id
	private Integer mmsi;

	@Indexed
	private Integer imo;

	@Indexed
	private String callSign;

	@Indexed
	private String name;

	@Indexed
	private int shipType;

	@Indexed
	private int dimensionA;

	@Indexed
	private int dimensionB;

	@Indexed
	private int dimensionC;

	@Indexed
	private int dimensionD;

	@Indexed
	private Date eta;

	@Indexed
	private double draught;

	@Indexed
	private String destination;

	@Indexed
	private int countryId;

	@Indexed
	private int messageSource;

	@Indexed
	@LastModifiedDate
	private Date timestamp;

	public Integer getMmsi() {
		return mmsi;
	}

	public void setMmsi(final Integer mmsi) {
		this.mmsi = mmsi;
	}

	public Integer getImo() {
		return imo;
	}

	public void setImo(final Integer imo) {
		this.imo = imo;
	}

	public String getCallSign() {
		return callSign;
	}

	public void setCallSign(final String callSign) {
		this.callSign = callSign;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public int getShipType() {
		return shipType;
	}

	public void setShipType(final int shipType) {
		this.shipType = shipType;
	}

	public int getDimensionA() {
		return dimensionA;
	}

	public void setDimensionA(final int dimensionA) {
		this.dimensionA = dimensionA;
	}

	public int getDimensionB() {
		return dimensionB;
	}

	public void setDimensionB(final int dimensionB) {
		this.dimensionB = dimensionB;
	}

	public int getDimensionC() {
		return dimensionC;
	}

	public void setDimensionC(final int dimensionC) {
		this.dimensionC = dimensionC;
	}

	public int getDimensionD() {
		return dimensionD;
	}

	public void setDimensionD(final int dimensionD) {
		this.dimensionD = dimensionD;
	}

	public Date getEta() {
		return eta;
	}

	public void setEta(final Date eta) {
		this.eta = eta;
	}

	public double getDraught() {
		return draught;
	}

	public void setDraught(final double draught) {
		this.draught = draught;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(final String destination) {
		this.destination = destination;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(final int countryId) {
		this.countryId = countryId;
	}

	public int getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(final int messageSource) {
		this.messageSource = messageSource;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(final Date timestamp) {
		this.timestamp = timestamp;
	}

}
