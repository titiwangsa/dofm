package com.twistlet.ttt.dofm.model.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("aisService")
public class AisServiceImp implements AisService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public int getType(final String encodedMessage) {
		if (encodedMessage == null || encodedMessage.isEmpty()) {
			logger.error("Empty AIS Content: [{}]");
			return -1;
		}
		final byte[] toDecBytes = encodedMessage.getBytes();
		final byte[] decBytes = ascii8To6bitBin(toDecBytes);
		if (decBytes == null) {
			logger.error("Invalid AIS Content: [{}]", encodedMessage);
			return -1;
		}
		final int msgId = decBytes[0];
		logger.debug("{} - {}", encodedMessage, msgId);
		return msgId;
	}

	private byte[] ascii8To6bitBin(final byte[] toDecBytes) {

		final byte[] convertedBytes = new byte[toDecBytes.length];
		int sum = 0;
		int _6bitBin = 0;

		for (int i = 0; i < toDecBytes.length; i++) {
			sum = 0;
			_6bitBin = 0;

			if (toDecBytes[i] < 48) {
				return null;
			}
			if (toDecBytes[i] > 119) {
				return null;
			}
			if (toDecBytes[i] > 87) {
				if (toDecBytes[i] < 96) {
					return null;
				}
				sum = toDecBytes[i] + 40;
			} else {
				sum = toDecBytes[i] + 40;
			}
			if (sum != 0) {
				if (sum > 128) {
					sum += 32;
				} else {
					sum += 40;
				}
				_6bitBin = sum & 0x3F;
				convertedBytes[i] = (byte) _6bitBin;
			}
		}
		return convertedBytes;
	}
}
