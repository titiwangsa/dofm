package com.twistlet.ttt.dofm.model.entity;

import java.util.Date;

import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;

public abstract class BasePosition {

	@Indexed
	private String id;

	@Indexed(unique = true)
	private String ais;

	@Indexed
	private int mmsi;

	private int messageId;

	private int repeatIndicator;

	private int navigationalStatus;

	private double rot;

	private double sog;

	private double cog;

	@Indexed
	private int trueHeading;

	@GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
	private double[] point;

	@Indexed
	private Date timestamp;

	private double[] nearestShorePoint;

	private double distanceToShore;

	@Indexed
	private Boolean fishingFleet;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getAis() {
		return ais;
	}

	public void setAis(final String ais) {
		this.ais = ais;
	}

	public int getMmsi() {
		return mmsi;
	}

	public void setMmsi(final int mmsi) {
		this.mmsi = mmsi;
	}

	public int getMessageId() {
		return messageId;
	}

	public void setMessageId(final int messageId) {
		this.messageId = messageId;
	}

	public int getRepeatIndicator() {
		return repeatIndicator;
	}

	public void setRepeatIndicator(final int repeatIndicator) {
		this.repeatIndicator = repeatIndicator;
	}

	public int getNavigationalStatus() {
		return navigationalStatus;
	}

	public void setNavigationalStatus(final int navigationalStatus) {
		this.navigationalStatus = navigationalStatus;
	}

	public double getRot() {
		return rot;
	}

	public void setRot(final double rot) {
		this.rot = rot;
	}

	public double getSog() {
		return sog;
	}

	public void setSog(final double sog) {
		this.sog = sog;
	}

	public double getCog() {
		return cog;
	}

	public void setCog(final double cog) {
		this.cog = cog;
	}

	public int getTrueHeading() {
		return trueHeading;
	}

	public void setTrueHeading(final int trueHeading) {
		this.trueHeading = trueHeading;
	}

	public double[] getPoint() {
		return point;
	}

	public void setPoint(final double[] point) {
		this.point = point;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(final Date timestamp) {
		this.timestamp = timestamp;
	}

	public double[] getNearestShorePoint() {
		return nearestShorePoint;
	}

	public void setNearestShorePoint(final double[] nearestShorePoint) {
		this.nearestShorePoint = nearestShorePoint;
	}

	public double getDistanceToShore() {
		return distanceToShore;
	}

	public void setDistanceToShore(final double distanceToShore) {
		this.distanceToShore = distanceToShore;
	}

	public Boolean getFishingFleet() {
		return fishingFleet;
	}

	public void setFishingFleet(final Boolean fishingFleet) {
		this.fishingFleet = fishingFleet;
	}
}
