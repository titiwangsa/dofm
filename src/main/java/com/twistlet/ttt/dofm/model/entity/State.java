package com.twistlet.ttt.dofm.model.entity;

import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

public class State {

	@Id
	private String name;

	@Indexed
	private Set<String> regions;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Set<String> getRegions() {
		return regions;
	}

	public void setRegions(final Set<String> regions) {
		this.regions = regions;
	}
}
