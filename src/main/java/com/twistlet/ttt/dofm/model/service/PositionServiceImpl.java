package com.twistlet.ttt.dofm.model.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.LocalShip;
import com.twistlet.ttt.dofm.model.entity.Position;
import com.twistlet.ttt.dofm.model.entity.Shoreline;
import com.twistlet.ttt.dofm.model.repository.LocalShipRepository;
import com.twistlet.ttt.dofm.model.repository.PositionRepository;
import com.twistlet.ttt.dofm.model.repository.ShorelineRepository;

@Service("positionService")
public class PositionServiceImpl implements PositionService {

	private final PositionRepository positionRepository;
	private final ShorelineRepository shorelineRepository;
	private final LocalShipRepository localShipRepository;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	public PositionServiceImpl(final PositionRepository positionRepository,
			final ShorelineRepository shorelineRepository,
			final LocalShipRepository localShipRepository) {
		this.positionRepository = positionRepository;
		this.shorelineRepository = shorelineRepository;
		this.localShipRepository = localShipRepository;
	}

	@Override
	public void save(final Position position) {
		final List<LocalShip> localShips = localShipRepository.findAll();
		final List<Integer> mmsiLocal = toMmsi(localShips);
		if (mmsiLocal.contains(position.getMmsi())) {
			saveFleet(position);
		} else {
			position.setFishingFleet(Boolean.FALSE);
			positionRepository.save(position);
		}
	}

	private List<Integer> toMmsi(final List<LocalShip> localShips) {
		final List<Integer> list = new ArrayList<>();
		for (final LocalShip localShip : localShips) {
			list.add(localShip.getMmsi());
		}
		return list;
	}

	private void saveFleet(final Position position) {
		final double location[] = position.getPoint();
		final GeoResults<Shoreline> result = shorelineRepository
				.findGeoResultsByPointNear(location[0], location[1]);
		final List<GeoResult<Shoreline>> content = result.getContent();
		if (content.size() == 0) {
			logger.error("No shoreline coordinates available");
			return;
		}
		final GeoResult<Shoreline> geoResult = content.get(0);
		final Shoreline item = geoResult.getContent();
		final Distance d = geoResult.getDistance();
		final double distance = d.getValue();

		final double[] coordinates = item.getPoint();
		position.setNearestShorePoint(coordinates);
		position.setDistanceToShore(distance);
		position.setFishingFleet(Boolean.TRUE);
		positionRepository.save(position);

	}

}
