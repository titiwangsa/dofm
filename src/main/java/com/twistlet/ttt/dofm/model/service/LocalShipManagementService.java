package com.twistlet.ttt.dofm.model.service;

import java.util.List;

import com.twistlet.ttt.dofm.model.entity.LocalShip;

public interface LocalShipManagementService {
	void save(LocalShip localShip);

	LocalShip get(Integer id);

	List<LocalShip> list();

	void remove(Integer id);

}
