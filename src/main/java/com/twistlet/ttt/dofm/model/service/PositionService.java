package com.twistlet.ttt.dofm.model.service;

import com.twistlet.ttt.dofm.model.entity.Position;

public interface PositionService {

	void save(Position position);
}
