package com.twistlet.ttt.dofm.model.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.twistlet.ttt.dofm.model.entity.Role;

public interface RoleRepository extends MongoRepository<Role, String> {

}
