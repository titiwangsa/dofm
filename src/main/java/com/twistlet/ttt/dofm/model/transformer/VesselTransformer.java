package com.twistlet.ttt.dofm.model.transformer;

import org.freeais.ais.AISVessel;
import org.springframework.stereotype.Component;

import com.twistlet.ttt.dofm.model.entity.Vessel;

@Component("vesselTransformer")
public class VesselTransformer {

	public Vessel transform(final AISVessel source) {
		final Vessel target = new Vessel();
		target.setImo(source.getImo());
		target.setMmsi(source.getMmsi());
		target.setCallSign(source.getCallSign());
		target.setName(source.getName());
		target.setShipType(source.getShipType());
		target.setDimensionA(source.getDimensionA());
		target.setDimensionB(source.getDimensionB());
		target.setDimensionC(source.getDimensionC());
		target.setDimensionD(source.getDimensionD());
		target.setEta(source.getEta());
		target.setDraught(source.getDraught());
		target.setDestination(source.getDestination());
		target.setCountryId(source.getCountryId());
		target.setMessageSource(source.getMsgSrc());
		return target;
	}

}
