package com.twistlet.ttt.dofm.model.service;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.User;
import com.twistlet.ttt.dofm.model.repository.SpringSecurityUserDetails;
import com.twistlet.ttt.dofm.model.repository.UserRepository;

@Service("userDetailsService")
public class SecurityUserDetailsService implements UserDetailsService {

	private final UserRepository userRepository;

	@Autowired
	public SecurityUserDetailsService(final UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(final String username)
			throws UsernameNotFoundException {
		final User user = userRepository.findOne(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		final SpringSecurityUserDetails userDetails = new SpringSecurityUserDetails();
		userDetails.setUsername(user.getUsername());
		userDetails.setPassword(user.getPassword());
		userDetails.setAuthorities(toAuthorities(user.getRole()));
		userDetails.setEnabled(user.getEnabled());
		return userDetails;
	}

	private Collection<? extends GrantedAuthority> toAuthorities(
			final String role) {
		final Set<GrantedAuthority> set = new LinkedHashSet<>();
		set.add(new SimpleGrantedAuthority(role));
		return set;
	}
}
