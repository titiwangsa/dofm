package com.twistlet.ttt.dofm.model.transformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.transformer.Transformer;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.twistlet.ttt.dofm.model.service.AisService;

@Component("addMessageTypeIdTransformer")
public class AddMessageTypeIdTransformer implements Transformer {

	private final AisService aisService;
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	public AddMessageTypeIdTransformer(final AisService aisService) {
		this.aisService = aisService;
	}

	@Override
	public Message<?> transform(final Message<?> message) {
		final MessageBuilder<?> builder = MessageBuilder.fromMessage(message);
		final String line = message.getPayload().toString();
		final int messageType = aisService.getType(line);
		builder.setHeader("MESSAGE_TYPE", messageType);
		builder.setHeader("ORIGINAL_AIS", line);
		final Message<?> newMessage = builder.build();
		return newMessage;
	}
}
