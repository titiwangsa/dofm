package com.twistlet.ttt.dofm.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.State;
import com.twistlet.ttt.dofm.model.repository.StateRepository;

@Service
public class StateManagementServiceImpl implements StateManagementService {

	private final StateRepository stateRepository;

	@Autowired
	public StateManagementServiceImpl(final StateRepository stateRepository) {
		this.stateRepository = stateRepository;
	}

	@Override
	public void save(final State state) {
		stateRepository.save(state);
	}

	@Override
	public List<State> list() {
		return stateRepository.findAll();
	}

	@Override
	public State get(final String id) {
		return stateRepository.findOne(id);
	}

	@Override
	public void remove(final String id) {
		stateRepository.delete(id);
	}

}
