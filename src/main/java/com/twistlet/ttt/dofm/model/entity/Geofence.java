package com.twistlet.ttt.dofm.model.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "geofence")
public class Geofence {

	@Id
	private String id;

	private String label;

	@GeoSpatialIndexed
	private List<double[]> points;

	private boolean closed;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public List<double[]> getPoints() {
		return points;
	}

	public void setPoints(final List<double[]> points) {
		this.points = points;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(final boolean closed) {
		this.closed = closed;
	}

}
