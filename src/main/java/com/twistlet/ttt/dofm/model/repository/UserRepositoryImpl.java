package com.twistlet.ttt.dofm.model.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.twistlet.ttt.dofm.model.entity.User;

@Repository
public class UserRepositoryImpl implements UserRepositoryCustom {

	private MongoOperations mongoOperations;

	@Autowired
	public UserRepositoryImpl(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	@Override
	public void updatePassword(String username, String hashed) {
		Query query = new Query(Criteria.where("username").is(username));
		Update update = new Update().set("password", hashed);
		mongoOperations.findAndModify(query, update, User.class);
	}

	@Override
	public void update(String username, String fullName, String role,
			Boolean enabled) {
		Query query = new Query(Criteria.where("username").is(username));
		Update update = new Update();
		update.set("fullName", fullName);
		update.set("role", role);
		update.set("enabled", enabled);
		mongoOperations.findAndModify(query, update, User.class);
	}
}
