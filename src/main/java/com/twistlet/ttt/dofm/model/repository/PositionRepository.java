package com.twistlet.ttt.dofm.model.repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.twistlet.ttt.dofm.model.entity.Position;

@Repository("positionRepository")
public interface PositionRepository extends MongoRepository<Position, Integer>,
		PositionRepositoryCustom {

	List<Position> findBytimestampGreaterThanOrMmsiIn(Date timestamp,
			Collection<Integer> list);

}
