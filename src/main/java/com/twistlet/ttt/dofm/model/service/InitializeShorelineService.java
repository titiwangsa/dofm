package com.twistlet.ttt.dofm.model.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.Shoreline;
import com.twistlet.ttt.dofm.model.repository.ShorelineRepository;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.LineString;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

@Service("initializeShorelineService")
public class InitializeShorelineService implements InitializationService {

	private final ShorelineRepository shorelineRepository;
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	public InitializeShorelineService(
			final ShorelineRepository shorelineRepository) {
		this.shorelineRepository = shorelineRepository;
	}

	@Override
	public void initialize() {
		if (shorelineRepository.count() == 0) {
			try {
				final String path = "kml/shoreline.kml.xml";
				final Resource resource = new ClassPathResource(path);
				final InputStream inputStream = resource.getInputStream();
				populate(inputStream);
				IOUtils.closeQuietly(inputStream);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void populate(final InputStream inputStream) {
		logger.info("Populating shoreline. This will take a very long time. Please wait.");
		final Kml kml = Kml.unmarshal(inputStream);
		final Document document = (Document) kml.getFeature();
		final List<Feature> list = document.getFeature();
		int groupId = 0;
		int total = 0;
		logger.info("Processing {} features.", list.size());
		for (final Feature feature : list) {
			if (feature instanceof Placemark) {
				final Placemark placemark = (Placemark) feature;
				final LineString lineString = (LineString) placemark
						.getGeometry();
				final List<Coordinate> coordinates = lineString
						.getCoordinates();
				if (coordinates.size() == 0) {
					continue;
				}
				int index = 0;
				List<Shoreline> shorelines = new ArrayList<>();
				logger.info("Processing {} coordinates.", coordinates.size());
				for (final Coordinate coordinate : coordinates) {
					final Shoreline shoreline = new Shoreline();
					shoreline.setGroupId(groupId);
					shoreline.setSequenceId(index);
					final double[] point = { coordinate.getLongitude(),
							coordinate.getLatitude() };
					shoreline.setPoint(point);
					shorelines.add(shoreline);
					total++;
					index++;
				}
				shorelineRepository.save(shorelines);
				final String message = "group {}, {} items inserted";
				logger.info(message, new Object[] { groupId, index });
				groupId++;
			}
		}
		logger.info("{} shorelines inserted", total);
	}

}
