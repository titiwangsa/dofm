package com.twistlet.ttt.dofm.model.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.twistlet.ttt.dofm.model.entity.State;

@Repository
public interface StateRepository extends MongoRepository<State, String>,
		StateRepositoryCustom {

}
