package com.twistlet.ttt.dofm.model.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.twistlet.ttt.dofm.model.entity.Raw;

@Repository
public interface RawRepository extends MongoRepository<Raw, String> {

}
