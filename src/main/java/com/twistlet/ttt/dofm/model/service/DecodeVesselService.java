package com.twistlet.ttt.dofm.model.service;

import org.freeais.ais.AISVessel;

public interface DecodeVesselService {
	AISVessel decode(String section);
}
