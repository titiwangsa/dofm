package com.twistlet.ttt.dofm.model.service;

import com.twistlet.ttt.dofm.model.entity.Position;
import com.twistlet.ttt.dofm.model.entity.Vessel;

public class IdPositionVessel {

	private int id;
	private Position position;
	private Vessel vessel;

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(final Position position) {
		this.position = position;
	}

	public Vessel getVessel() {
		return vessel;
	}

	public void setVessel(final Vessel vessel) {
		this.vessel = vessel;
	}

}
