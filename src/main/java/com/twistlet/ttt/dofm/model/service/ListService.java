package com.twistlet.ttt.dofm.model.service;

import java.util.List;

public interface ListService {

	List<IdPositionVessel> listPositionVessel();
}
