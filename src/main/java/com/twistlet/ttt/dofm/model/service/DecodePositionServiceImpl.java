package com.twistlet.ttt.dofm.model.service;

import org.freeais.ais.AISDecoder;
import org.freeais.ais.AISParseException;
import org.freeais.ais.AISPositionA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("decodePositionService")
public class DecodePositionServiceImpl implements DecodePositionService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public AISPositionA decode(final String section) {
		try {
			final AISPositionA position = (AISPositionA) AISDecoder
					.decode(section);
			return position;
		} catch (final AISParseException e) {
			logger.debug("{} - {}", e.toString(), section);
		}
		return null;
	}

}
