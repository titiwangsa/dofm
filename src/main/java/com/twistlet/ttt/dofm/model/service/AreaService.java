package com.twistlet.ttt.dofm.model.service;

import java.util.Set;

public interface AreaService {

	void set(String id, Set<String> regions);

	Set<String> listAreas(String id);
}
