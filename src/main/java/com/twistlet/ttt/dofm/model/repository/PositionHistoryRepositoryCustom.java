package com.twistlet.ttt.dofm.model.repository;

import java.util.Date;
import java.util.List;

import com.twistlet.ttt.dofm.model.entity.PositionHistory;

public interface PositionHistoryRepositoryCustom {
	List<PositionHistory> findByMmsiAndDateRange(List<Integer> listMmsi,
			Date from, Date to);
}