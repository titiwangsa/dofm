package com.twistlet.ttt.dofm.model.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.twistlet.ttt.dofm.model.entity.User;

public interface UserRepository extends MongoRepository<User, String>,
		UserRepositoryCustom {

	List<User> findByRole(String role);

}
