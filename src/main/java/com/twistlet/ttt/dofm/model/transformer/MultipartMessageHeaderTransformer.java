package com.twistlet.ttt.dofm.model.transformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.transformer.Transformer;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component("multipartMessageHeaderTransformer")
public class MultipartMessageHeaderTransformer implements Transformer {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public Message<?> transform(final Message<?> message) {
		final MessageBuilder<?> messageBuilder = MessageBuilder
				.fromMessage(message);
		final Object payload = message.getPayload();
		final String line = payload.toString();
		logger.debug("Received : {}", line);
		final String fields[] = line.split(",");
		final String sequenceSize = fields[1];
		final String sequenceNumber = fields[2];
		final String correlationId = fields[3];
		messageBuilder.setCorrelationId(correlationId);
		messageBuilder.setSequenceNumber(new Integer(sequenceNumber));
		messageBuilder.setSequenceSize(new Integer(sequenceSize));
		return messageBuilder.build();
	}

}
