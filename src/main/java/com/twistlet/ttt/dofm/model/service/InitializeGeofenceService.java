package com.twistlet.ttt.dofm.model.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.Geofence;
import com.twistlet.ttt.dofm.model.repository.GeofenceRepository;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.LineString;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

@Service("initializeGeofenceService")
public class InitializeGeofenceService implements InitializationService {

	private final GeofenceRepository geofenceRepository;
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	public InitializeGeofenceService(final GeofenceRepository geofenceRepository) {
		this.geofenceRepository = geofenceRepository;
	}

	@Override
	public void initialize() {
		if (geofenceRepository.count() == 0) {
			try {
				final String path = "kml/geofence_5nm.kml.xml";
				final Resource resource = new ClassPathResource(path);
				final InputStream inputStream = resource.getInputStream();
				populate(inputStream);
				IOUtils.closeQuietly(inputStream);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void populate(final InputStream inputStream) {
		final Kml kml = Kml.unmarshal(inputStream);
		final Document document = (Document) kml.getFeature();
		final List<Feature> list = document.getFeature();
		geofenceRepository.deleteAll();
		List<Geofence> geofences = new ArrayList<Geofence>();
		for (final Feature feature : list) {
			if (feature instanceof Placemark) {
				final Placemark placemark = (Placemark) feature;
				final LineString lineString = (LineString) placemark
						.getGeometry();
				final List<Coordinate> coordinates = lineString
						.getCoordinates();
				final Geofence geofence = new Geofence();
				final List<double[]> points = new ArrayList<>();
				for (final Coordinate coordinate : coordinates) {
					final double[] point = new double[] {
							coordinate.getLongitude(), coordinate.getLatitude() };
					points.add(point);
				}
				geofence.setPoints(points);
				geofence.setLabel("5 nm");
				geofences.add(geofence);
			}
		}
		geofenceRepository.save(geofences);
		logger.info("Geofence 5nm stored");
	}
}
