package com.twistlet.ttt.dofm.model.transformer;

import org.freeais.ais.AISPositionA;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.transformer.Transformer;
import org.springframework.messaging.Message;

import com.twistlet.ttt.dofm.model.entity.BasePosition;

public abstract class BasePositionTransformer implements Transformer {

	@Override
	public Message<?> transform(final Message<?> message) {
		final AISPositionA source = (AISPositionA) message.getPayload();
		final BasePosition target = createBasePosition();
		target.setAis(message.getHeaders().get("ORIGINAL_AIS").toString());
		target.setMmsi(source.getMmsi());
		target.setMessageId(source.getMsgId());
		target.setRepeatIndicator(source.getRepeatIndicator());
		target.setNavigationalStatus(source.getNavState());
		target.setRot(source.getRot());
		target.setSog(source.getSog());
		target.setCog(source.getCog());
		target.setTrueHeading(source.getTrueHeading());
		target.setPoint(new double[] { source.getLongitude(),
				source.getLatitude() });
		target.setTimestamp(source.getMsgTimestamp());

		final MessageBuilder<BasePosition> messageBuilder = MessageBuilder
				.withPayload(target);
		messageBuilder.copyHeaders(message.getHeaders());
		return messageBuilder.build();
	}

	protected abstract BasePosition createBasePosition();

}
