package com.twistlet.ttt.dofm.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Position extends BasePosition {

	@Override
	public void setMmsi(final int mmsi) {
		super.setMmsi(mmsi);
		setId(Integer.toString(mmsi));
	}

}
