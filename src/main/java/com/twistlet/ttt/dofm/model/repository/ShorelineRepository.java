package com.twistlet.ttt.dofm.model.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.twistlet.ttt.dofm.model.entity.Shoreline;

public interface ShorelineRepository extends
		MongoRepository<Shoreline, String>, ShorelineRepositoryCustom {
	List<Shoreline> findByGroupIdOrderBySequenceIdAsc(int groupId);
}
