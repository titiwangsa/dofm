package com.twistlet.ttt.dofm.model.repository;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.twistlet.ttt.dofm.model.entity.State;

@Repository
public class StateRepositoryImpl implements StateRepositoryCustom {

	private final MongoOperations mongoOperations;

	@Autowired
	public StateRepositoryImpl(final MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	@Override
	public void updateArea(final String id, final Set<String> regions) {
		final Query query = new Query(Criteria.where("name").is(id));
		final Update update = new Update().set("regions", regions);
		mongoOperations.findAndModify(query, update, State.class);
	}
}
