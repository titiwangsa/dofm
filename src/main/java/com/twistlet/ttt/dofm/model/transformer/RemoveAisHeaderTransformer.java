package com.twistlet.ttt.dofm.model.transformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.transformer.Transformer;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component("removeAisHeaderTransformer")
public class RemoveAisHeaderTransformer implements Transformer {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private final int INDEX_ENCODED_MESSAGE = 5;

	@Override
	public Message<?> transform(final Message<?> message) {
		final String payload = (String) message.getPayload();
		final String[] blocks = payload.split(",");
		final MessageBuilder<?> builder = MessageBuilder
				.withPayload(blocks[INDEX_ENCODED_MESSAGE]);
		builder.copyHeaders(message.getHeaders());
		return builder.build();
	}
}
