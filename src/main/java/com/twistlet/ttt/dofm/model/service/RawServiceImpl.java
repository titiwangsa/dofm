package com.twistlet.ttt.dofm.model.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.Raw;
import com.twistlet.ttt.dofm.model.repository.RawRepository;

@Service("rawService")
public class RawServiceImpl implements RawService {

	private RawRepository rawRepository;

	@Autowired
	public RawServiceImpl(RawRepository rawRepository) {
		this.rawRepository = rawRepository;
	}

	@Override
	public void save(String line) {
		Raw raw = new Raw();
		raw.setData(line);
		raw.setTimestamp(new Date());
		rawRepository.save(raw);
	}

}
