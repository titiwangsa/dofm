package com.twistlet.ttt.dofm.model.repository;

import java.util.Set;

public interface StateRepositoryCustom {

	void updateArea(String id, Set<String> regions);

}
