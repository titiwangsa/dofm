package com.twistlet.ttt.dofm.model.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.PositionHistory;
import com.twistlet.ttt.dofm.model.repository.PositionHistoryRepository;

@Service
public class PlaybackServiceImpl implements PlaybackService {

	private final PositionHistoryRepository positionHistoryRepository;

	@Autowired
	public PlaybackServiceImpl(
			final PositionHistoryRepository positionHistoryRepository) {
		this.positionHistoryRepository = positionHistoryRepository;
	}

	@Override
	public List<PositionHistory> list(final List<Integer> listMmsi,
			final Date dateFrom, final Date dateTo) {
		return positionHistoryRepository.findByMmsiAndDateRange(listMmsi,
				dateFrom, dateTo);
	}

}
