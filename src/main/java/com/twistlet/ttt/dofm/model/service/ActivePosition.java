package com.twistlet.ttt.dofm.model.service;

import java.util.Date;

import org.springframework.data.geo.Point;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ActivePosition {

	private Point point;
	private int mmsi;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy hh:mm:ss a", timezone = "GMT+8")
	private Date lastModifiedDate;

	private Long lastModifiedTimeStamp;

	private Boolean fishingFleet;

	private int degree;

	private String name;

	private double cog;

	private double distanceToShore = 0.00;

	private double sog;

	private Point pointToShore;

	public Point getPoint() {
		return point;
	}

	public void setPoint(final Point point) {
		this.point = point;
	}

	public int getMmsi() {
		return mmsi;
	}

	public void setMmsi(final int mmsi) {
		this.mmsi = mmsi;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(final Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Long getLastModifiedTimeStamp() {
		return lastModifiedTimeStamp;
	}

	public void setLastModifiedTimeStamp(final Long lastModifiedTimeStamp) {
		this.lastModifiedTimeStamp = lastModifiedTimeStamp;
	}

	public Boolean getFishingFleet() {
		return fishingFleet;
	}

	public void setFishingFleet(final Boolean fishingFleet) {
		this.fishingFleet = fishingFleet;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(final int degree) {
		this.degree = degree;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public double getCog() {
		return cog;
	}

	public void setCog(final double cog) {
		this.cog = cog;
	}

	public double getDistanceToShore() {
		return distanceToShore;
	}

	public void setDistanceToShore(final double distanceToShore) {
		this.distanceToShore = distanceToShore;
	}

	public double getSog() {
		return sog;
	}

	public void setSog(final double sog) {
		this.sog = sog;
	}

	public Point getPointToShore() {
		return pointToShore;
	}

	public void setPointToShore(final Point pointToShore) {
		this.pointToShore = pointToShore;
	}

}
