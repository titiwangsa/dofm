package com.twistlet.ttt.dofm.model.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.Role;
import com.twistlet.ttt.dofm.model.entity.User;
import com.twistlet.ttt.dofm.model.repository.RoleRepository;
import com.twistlet.ttt.dofm.model.repository.UserRepository;

@Service("initializeAuthenticationUser")
public class InitializeAuthenticationUser implements InitializationService {

	private final UserRepository userRepository;
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private final RoleRepository roleRepository;
	private final PasswordEncoder passwordEncoder;

	@Autowired
	public InitializeAuthenticationUser(final UserRepository userRepository,
			final RoleRepository roleRepository,
			final PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public void initialize() {
		initializeUser();
		initializeRole();

	}

	private void initializeRole() {
		final long count = roleRepository.count();
		if (count == 0) {
			logger.info("Creating admin role");
			roleRepository.save(new Role("ROLE_ADMIN"));
			roleRepository.save(new Role("ROLE_DOFM"));
			roleRepository.save(new Role("ROLE_STATE"));
			roleRepository.save(new Role("ROLE_REGION"));
			roleRepository.save(new Role("ROLE_OWNER"));
		}
	}

	private void initializeUser() {
		final long count = userRepository.count();
		if (count == 0) {
			logger.info("Creating new user");
			userRepository.save(createNewUserAdmin());
			userRepository.save(createNewUserDofm());
		}
	}

	private User createNewUserDofm() {
		final User user = new User();
		user.setFullName("Pegawai DOFM");
		user.setUsername("dofm");
		user.setEnabled(Boolean.TRUE);
		user.setRole("ROLE_DOFM");
		final String password = "dofm32dofm";
		final String hashed = passwordEncoder.encode(password);
		user.setPassword(hashed);
		return user;
	}

	private User createNewUserAdmin() {
		final User user = new User();
		user.setFullName("Administrator");
		user.setUsername("admin");
		user.setEnabled(Boolean.TRUE);
		user.setRole("ROLE_ADMIN");
		final String password = "garage07";
		final String hashed = passwordEncoder.encode(password);
		user.setPassword(hashed);
		return user;
	}

}
