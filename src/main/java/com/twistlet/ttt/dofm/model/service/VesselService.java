package com.twistlet.ttt.dofm.model.service;

import java.util.List;
import java.util.Map;

public interface VesselService {
	Map<Integer, String> findShipName(List<Integer> listMmsi);
}
