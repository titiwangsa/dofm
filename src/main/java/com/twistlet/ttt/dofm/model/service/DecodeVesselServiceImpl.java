package com.twistlet.ttt.dofm.model.service;

import org.freeais.ais.AISDecoder;
import org.freeais.ais.AISParseException;
import org.freeais.ais.AISVessel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("decodeVesselService")
public class DecodeVesselServiceImpl implements DecodeVesselService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public AISVessel decode(String section) {
		try {
			Object object = AISDecoder.decode(section);
			if (object instanceof AISVessel) {
				return (AISVessel) object;
			} else {
				return null;
			}
		} catch (final AISParseException e) {
			logger.debug("{} - {}", e.toString(), section);
		}
		return null;
	}
}
