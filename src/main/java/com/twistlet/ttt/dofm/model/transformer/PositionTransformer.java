package com.twistlet.ttt.dofm.model.transformer;

import org.springframework.stereotype.Component;

import com.twistlet.ttt.dofm.model.entity.BasePosition;
import com.twistlet.ttt.dofm.model.entity.Position;

@Component("positionTransformer")
public class PositionTransformer extends BasePositionTransformer {

	@Override
	protected BasePosition createBasePosition() {
		return new Position();
	}

}
