package com.twistlet.ttt.dofm.model.repository;

public interface UserRepositoryCustom {
	void updatePassword(String username, String hashed);

	void update(String username, String fullName, String role, Boolean enabled);
}
