package com.twistlet.ttt.dofm.model.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.twistlet.ttt.dofm.model.entity.Geofence;

public interface GeofenceRepository extends MongoRepository<Geofence, String> {

}
