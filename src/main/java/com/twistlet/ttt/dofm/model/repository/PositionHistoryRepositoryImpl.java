package com.twistlet.ttt.dofm.model.repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.twistlet.ttt.dofm.model.entity.PositionHistory;

@Repository
public class PositionHistoryRepositoryImpl implements
		PositionHistoryRepositoryCustom {

	private final MongoTemplate mongoTemplate;

	@Autowired
	public PositionHistoryRepositoryImpl(final MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public List<PositionHistory> findByMmsiAndDateRange(
			final List<Integer> listMmsi, final Date from, final Date to) {
		final Criteria criteria = where("mmsi").in(listMmsi);
		criteria.and("timestamp").gte(from).lte(to);
		final Query query = new Query(criteria);
		return mongoTemplate.find(query, PositionHistory.class);
	}

}
