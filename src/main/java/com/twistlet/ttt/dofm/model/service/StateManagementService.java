package com.twistlet.ttt.dofm.model.service;

import java.util.List;

import com.twistlet.ttt.dofm.model.entity.State;

public interface StateManagementService {
	void save(State state);

	State get(String id);

	List<State> list();

	void remove(String id);

}
