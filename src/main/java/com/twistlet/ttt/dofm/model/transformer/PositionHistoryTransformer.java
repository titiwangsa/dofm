package com.twistlet.ttt.dofm.model.transformer;

import org.springframework.stereotype.Component;

import com.twistlet.ttt.dofm.model.entity.BasePosition;
import com.twistlet.ttt.dofm.model.entity.PositionHistory;

@Component("positionHistoryTransformer")
public class PositionHistoryTransformer extends BasePositionTransformer {

	@Override
	protected BasePosition createBasePosition() {
		return new PositionHistory();
	}

}
