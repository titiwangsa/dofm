package com.twistlet.ttt.dofm.model.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.twistlet.ttt.dofm.model.entity.Vessel;

@Repository
public interface VesselRepository extends MongoRepository<Vessel, Integer> {
	List<Vessel> findByMmsiIn(List<Integer> listOfMmsi);
}
