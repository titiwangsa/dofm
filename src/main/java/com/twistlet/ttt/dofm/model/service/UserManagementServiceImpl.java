package com.twistlet.ttt.dofm.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.User;
import com.twistlet.ttt.dofm.model.repository.UserRepository;

@Service
public class UserManagementServiceImpl implements UserManagementService {

	private PasswordEncoder passwordEncoder;
	private MongoTemplate mongoTemplate;
	private UserRepository userRepository;

	@Autowired
	public UserManagementServiceImpl(PasswordEncoder passwordEncoder,
			MongoTemplate mongoTemplate, UserRepository userRepository) {
		this.passwordEncoder = passwordEncoder;
		this.mongoTemplate = mongoTemplate;
		this.userRepository = userRepository;
	}

	@Override
	public void save(String username, String name, String password, String role) {
		String hashed = passwordEncoder.encode(password);
		User user = new User();
		user.setEnabled(Boolean.TRUE);
		user.setFullName(name);
		user.setPassword(hashed);
		user.setRole(role);
		user.setUsername(username);
		mongoTemplate.insert(user);
	}

	@Override
	public User get(String username) {
		return userRepository.findOne(username);
	}

	@Override
	public List<User> list() {
		return userRepository.findAll();
	}

	@Override
	public void remove(String id) {
		userRepository.delete(id);
	}

	@Override
	public void changePassword(String username, String password) {
		String hashed = passwordEncoder.encode(password);
		userRepository.updatePassword(username, hashed);
	}

	@Override
	public void update(String username, String fullName, String role,
			Boolean enabled) {
		userRepository.update(username, fullName, role, enabled);
	}

	@Override
	public List<User> listOwner() {
		return userRepository.findByRole("ROLE_OWNER");
	}
}
