package com.twistlet.ttt.dofm.model.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.Geofence;
import com.twistlet.ttt.dofm.model.entity.LocalShip;
import com.twistlet.ttt.dofm.model.entity.Position;
import com.twistlet.ttt.dofm.model.entity.Shoreline;
import com.twistlet.ttt.dofm.model.entity.Vessel;
import com.twistlet.ttt.dofm.model.repository.GeofenceRepository;
import com.twistlet.ttt.dofm.model.repository.LocalShipRepository;
import com.twistlet.ttt.dofm.model.repository.PositionRepository;
import com.twistlet.ttt.dofm.model.repository.ShorelineRepository;
import com.twistlet.ttt.dofm.model.repository.VesselRepository;

@Service
public class MapServiceImpl implements MapService {

	private final PositionRepository positionRepository;
	private final LocalShipRepository localShipRepository;
	private final VesselRepository vesselRepository;
	private final ShorelineRepository shorelineRepository;
	private final GeofenceRepository geofenceRepository;

	@Autowired
	public MapServiceImpl(final PositionRepository positionRepository,
			final LocalShipRepository localShipRepository,
			final VesselRepository vesselRepository,
			final ShorelineRepository shorelineRepository,
			final GeofenceRepository geofenceRepository) {
		this.positionRepository = positionRepository;
		this.localShipRepository = localShipRepository;
		this.vesselRepository = vesselRepository;
		this.shorelineRepository = shorelineRepository;
		this.geofenceRepository = geofenceRepository;
	}

	@Override
	public List<ActivePosition> listVessel() {
		final List<LocalShip> listLocal = localShipRepository.findAll();
		final List<Integer> listLocalId = new ArrayList<>();
		for (final LocalShip localShip : listLocal) {
			listLocalId.add(localShip.getMmsi());
		}
		final Date now = new Date();
		final Date date = DateUtils.addMinutes(now, -30);
		final List<Position> list = positionRepository
				.findBytimestampGreaterThanOrMmsiIn(date, listLocalId);
		final List<Integer> listOfMmsi = toListOfMmsi(list);
		final List<Vessel> listOfVessel = vesselRepository
				.findByMmsiIn(listOfMmsi);
		final Map<Integer, Vessel> map = toMap(listOfVessel);
		final List<ActivePosition> items = new ArrayList<>();
		for (final Position position : list) {
			final Vessel vessel = map.get(position.getMmsi());
			final Date timestamp = position.getTimestamp();
			final ActivePosition item = new ActivePosition();
			item.setMmsi(position.getMmsi());
			final double location[] = position.getPoint();
			item.setPoint(new Point(location[1], location[0]));
			item.setFishingFleet(position.getFishingFleet());
			if (Boolean.TRUE.equals(item.getFishingFleet())) {
				item.setDistanceToShore(position.getDistanceToShore());
				final double[] point = position.getNearestShorePoint();
				if (point != null & point.length == 2) {
					item.setPointToShore(new Point(point[1], point[0]));
				}
			}
			item.setLastModifiedDate(timestamp);
			item.setLastModifiedTimeStamp(timestamp.getTime());
			item.setDegree(position.getTrueHeading());
			if (vessel != null) {
				item.setName(vessel.getName());
			}
			item.setCog(position.getCog());
			item.setSog(position.getSog());
			items.add(item);
		}
		return items;
	}

	private Map<Integer, Vessel> toMap(final List<Vessel> listOfVessel) {
		final Map<Integer, Vessel> map = new LinkedHashMap<>();
		for (final Vessel vessel : listOfVessel) {
			map.put(vessel.getMmsi(), vessel);
		}
		return map;
	}

	private List<Integer> toListOfMmsi(final List<Position> positions) {
		final List<Integer> list = new ArrayList<>();
		for (final Position position : positions) {
			list.add(position.getMmsi());
		}
		return list;
	}

	@Override
	public List<Point> listShoreline(final int groupId) {
		final List<Point> list = new ArrayList<>();
		final List<Shoreline> items = shorelineRepository
				.findByGroupIdOrderBySequenceIdAsc(groupId);
		for (final Shoreline item : items) {
			final double[] point = item.getPoint();
			list.add(new Point(point[1], point[0]));
		}
		return list;
	}

	@Override
	public List<List<Point>> listShorelines() {
		final Map<Integer, List<Point>> map = new TreeMap<>();
		final Sort sort = new Sort(new Sort.Order("groupId"), new Sort.Order(
				"sequenceId"));
		final List<Shoreline> items = shorelineRepository.findAll(sort);
		for (final Shoreline shoreline : items) {
			final Integer groupId = shoreline.getGroupId();
			List<Point> points = map.get(groupId);
			if (points == null) {
				points = new ArrayList<Point>();
				map.put(groupId, points);
			}
			final double[] point = shoreline.getPoint();
			points.add(new Point(point[1], point[0]));
		}
		final List<List<Point>> list = new ArrayList<>(map.values());
		return list;
	}

	@Override
	public List<Geofence> listGeofences() {
		return geofenceRepository.findAll();
	}
}
