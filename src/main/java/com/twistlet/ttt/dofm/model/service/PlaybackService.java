package com.twistlet.ttt.dofm.model.service;

import java.util.Date;
import java.util.List;

import com.twistlet.ttt.dofm.model.entity.PositionHistory;

public interface PlaybackService {
	List<PositionHistory> list(List<Integer> listMmsi, Date dateFrom,
			Date dateTo);
}
