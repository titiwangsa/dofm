package com.twistlet.ttt.dofm.model.service;

import java.util.List;

import com.twistlet.ttt.dofm.model.entity.User;

public interface UserManagementService {
	void save(String username, String name, String password, String role);

	User get(String username);

	List<User> list();

	void remove(String id);

	void changePassword(String username, String password);

	void update(String username, String fullName, String role, Boolean enabled);

	List<User> listOwner();
}
