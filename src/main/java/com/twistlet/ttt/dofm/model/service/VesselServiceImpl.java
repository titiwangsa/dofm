package com.twistlet.ttt.dofm.model.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.Vessel;
import com.twistlet.ttt.dofm.model.repository.VesselRepository;

@Service
public class VesselServiceImpl implements VesselService {

	private final VesselRepository vesselRepository;

	@Autowired
	public VesselServiceImpl(final VesselRepository vesselRepository) {
		this.vesselRepository = vesselRepository;
	}

	@Override
	public Map<Integer, String> findShipName(final List<Integer> listMmsi) {
		final List<Vessel> list = vesselRepository.findByMmsiIn(listMmsi);
		final Map<Integer, String> map = new LinkedHashMap<>();
		for (final Vessel item : list) {
			final int id = item.getMmsi();
			final String name = item.getName();
			map.put(id, name);
		}
		return map;
	}

}
