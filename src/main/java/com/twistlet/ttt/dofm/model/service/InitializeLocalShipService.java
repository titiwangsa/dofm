package com.twistlet.ttt.dofm.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.LocalShip;
import com.twistlet.ttt.dofm.model.repository.LocalShipRepository;

@Service("initializeLocalShipService")
public class InitializeLocalShipService implements InitializationService {

	private final LocalShipRepository localShipRepository;

	@Autowired
	public InitializeLocalShipService(
			final LocalShipRepository localShipRepository) {
		super();
		this.localShipRepository = localShipRepository;
	}

	@Override
	public void initialize() {
		final long total = localShipRepository.count();
		if (total == 0) {
			populate();
		}
	}

	private void populate() {
		localShipRepository.save(create1());
		localShipRepository.save(create2());
		localShipRepository.save(create3());
		localShipRepository.save(create4());
		localShipRepository.save(create5());
		localShipRepository.save(create6());

	}

	private LocalShip create1() {
		final LocalShip localShip = new LocalShip();
		localShip.setMmsi(777999008);
		return localShip;
	}

	private LocalShip create2() {
		final LocalShip localShip = new LocalShip();
		localShip.setMmsi(777999012);
		return localShip;
	}

	private LocalShip create3() {
		final LocalShip localShip = new LocalShip();
		localShip.setMmsi(777999013);
		return localShip;
	}

	private LocalShip create4() {
		final LocalShip localShip = new LocalShip();
		localShip.setMmsi(777999009);
		return localShip;
	}

	private LocalShip create5() {
		final LocalShip localShip = new LocalShip();
		localShip.setMmsi(777220000);
		return localShip;
	}

	private LocalShip create6() {
		final LocalShip localShip = new LocalShip();
		localShip.setMmsi(777999011);
		return localShip;
	}

}
