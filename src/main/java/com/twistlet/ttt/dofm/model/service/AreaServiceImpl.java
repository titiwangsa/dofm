package com.twistlet.ttt.dofm.model.service;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.State;
import com.twistlet.ttt.dofm.model.repository.StateRepository;

@Service
public class AreaServiceImpl implements AreaService {

	private final StateRepository stateRepository;

	@Autowired
	public AreaServiceImpl(final StateRepository stateRepository) {
		this.stateRepository = stateRepository;
	}

	@Override
	public void set(final String id, final Set<String> regions) {
		stateRepository.updateArea(id, new TreeSet<String>(regions));
	}

	@Override
	public Set<String> listAreas(final String id) {
		final State item = stateRepository.findOne(id);
		if (item == null) {
			return Collections.emptySet();
		} else {
			final Set<String> regions = item.getRegions();
			return new TreeSet<String>(regions);
		}
	}

}
