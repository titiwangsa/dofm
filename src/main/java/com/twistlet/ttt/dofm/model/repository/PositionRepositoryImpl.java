package com.twistlet.ttt.dofm.model.repository;

import org.springframework.data.mongodb.core.MongoTemplate;

public class PositionRepositoryImpl implements PositionRepositoryCustom {

	protected final MongoTemplate mongoTemplate;

	public PositionRepositoryImpl(final MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

}
