package com.twistlet.ttt.dofm.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twistlet.ttt.dofm.model.entity.LocalShip;
import com.twistlet.ttt.dofm.model.repository.LocalShipRepository;

@Service
public class LocalShipManagementServiceImpl implements
		LocalShipManagementService {

	private final LocalShipRepository localShipRepository;

	@Autowired
	public LocalShipManagementServiceImpl(
			final LocalShipRepository localShipRepository) {
		this.localShipRepository = localShipRepository;
	}

	@Override
	public void save(final LocalShip localShip) {
		localShipRepository.save(localShip);
	}

	@Override
	public List<LocalShip> list() {
		return localShipRepository.findAll();
	}

	@Override
	public LocalShip get(final Integer id) {
		return localShipRepository.findOne(id);
	}

	@Override
	public void remove(final Integer id) {
		localShipRepository.delete(id);
	}

}
