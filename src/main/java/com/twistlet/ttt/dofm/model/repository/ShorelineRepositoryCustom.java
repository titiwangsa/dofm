package com.twistlet.ttt.dofm.model.repository;

import org.springframework.data.geo.GeoResults;

import com.twistlet.ttt.dofm.model.entity.Shoreline;

public interface ShorelineRepositoryCustom {

	GeoResults<Shoreline> findGeoResultsByPointNear(double lon, double lat);

}
