package com.twistlet.ttt.dofm.model.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Shoreline {

	@Id
	private String id;

	@Indexed
	private Integer sequenceId;

	@Indexed
	private Integer groupId;

	@GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
	private double[] point;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public Integer getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(final Integer sequenceId) {
		this.sequenceId = sequenceId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(final Integer groupId) {
		this.groupId = groupId;
	}

	public double[] getPoint() {
		return point;
	}

	public void setPoint(final double[] point) {
		this.point = point;
	}

}
