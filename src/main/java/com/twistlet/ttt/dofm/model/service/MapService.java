package com.twistlet.ttt.dofm.model.service;

import java.util.List;

import org.springframework.data.geo.Point;

import com.twistlet.ttt.dofm.model.entity.Geofence;

public interface MapService {

	List<ActivePosition> listVessel();

	List<Point> listShoreline(int groupId);

	List<List<Point>> listShorelines();

	List<Geofence> listGeofences();
}
