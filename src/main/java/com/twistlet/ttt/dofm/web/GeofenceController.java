package com.twistlet.ttt.dofm.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twistlet.ttt.dofm.model.entity.Geofence;
import com.twistlet.ttt.dofm.model.service.MapService;

@Controller
public class GeofenceController {

	private final MapService mapService;

	@Autowired
	public GeofenceController(final MapService mapService) {
		this.mapService = mapService;
	}

	@ResponseBody
	@RequestMapping("/geofences")
	public List<Geofence> get() {
		return mapService.listGeofences();
	}
}
