package com.twistlet.ttt.dofm.web;

import java.util.List;

import com.twistlet.ttt.dofm.model.service.ActivePosition;

public class CoordinateResult {

	private long timestamp;
	private List<ActivePosition> items;

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(final long timestamp) {
		this.timestamp = timestamp;
	}

	public List<ActivePosition> getItems() {
		return items;
	}

	public void setItems(final List<ActivePosition> items) {
		this.items = items;
	}

}
