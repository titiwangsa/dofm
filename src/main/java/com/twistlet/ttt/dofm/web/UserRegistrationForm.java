package com.twistlet.ttt.dofm.web;

import org.apache.commons.lang3.StringUtils;

public class UserRegistrationForm {

	private String username;
	private String name;
	private String password;
	private String confirmPassword;
	private String role;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isValid() {
		if (StringUtils.isBlank(username)) {
			return false;
		}
		if (StringUtils.isBlank(password)) {
			return false;
		}
		if (StringUtils.isBlank(name)) {
			return false;
		}
		if (!password.equals(confirmPassword)) {
			return false;
		}
		return true;
	}

}
