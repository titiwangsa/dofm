package com.twistlet.ttt.dofm.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.Lifecycle;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminAisController {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	private Lifecycle tcpInboundChannelAdapter;

	@Autowired
	public AdminAisController(
			@Qualifier("tcp-inbound-channel-adapter") Lifecycle tcpInboundChannelAdapter) {
		this.tcpInboundChannelAdapter = tcpInboundChannelAdapter;
	}

	@RequestMapping(value = "/admin/ais/status", method = RequestMethod.GET)
	public ModelAndView status() {
		Boolean isRunning = tcpInboundChannelAdapter.isRunning();
		ModelAndView mav = new ModelAndView();
		mav.addObject("isRunning", isRunning);
		return mav;
	}

	@RequestMapping(value = "/admin/ais/start", method = RequestMethod.POST)
	public ModelAndView start() {
		if (!tcpInboundChannelAdapter.isRunning()) {
			logger.info("Starting the AIS TCP Inbound Channel Adapter");
			tcpInboundChannelAdapter.start();
		}
		ModelAndView mav = new ModelAndView("redirect:/admin/ais/status");
		return mav;
	}

	@RequestMapping(value = "/admin/ais/stop", method = RequestMethod.POST)
	public ModelAndView stop() {
		if (tcpInboundChannelAdapter.isRunning()) {
			logger.info("Stopping the AIS TCP Inbound Channel Adapter");
			tcpInboundChannelAdapter.stop();
		}
		ModelAndView mav = new ModelAndView("redirect:/admin/ais/status");
		return mav;
	}
}
