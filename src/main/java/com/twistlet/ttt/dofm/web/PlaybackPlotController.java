package com.twistlet.ttt.dofm.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twistlet.ttt.dofm.model.entity.PositionHistory;
import com.twistlet.ttt.dofm.model.service.PlaybackService;
import com.twistlet.ttt.dofm.model.service.VesselService;

@Controller
public class PlaybackPlotController {

	private final PlaybackService playbackService;
	private final VesselService vesselService;

	@Autowired
	public PlaybackPlotController(final PlaybackService playbackService,
			final VesselService vesselService) {
		this.playbackService = playbackService;
		this.vesselService = vesselService;
	}

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		final SimpleDateFormat dateFormat = new SimpleDateFormat(
				"dd/MM/yyyy hh:mm:ss aa");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@ResponseBody
	@RequestMapping("/playback-plot")
	public PlaybackResult plot(final PlaybackRequest request) {
		final PlaybackResult result = new PlaybackResult();
		final List<PositionHistory> list = playbackService.list(
				request.getMmsi(), request.getFrom(), request.getTo());
		final List<PlaybackItem> items = new ArrayList<>();
		final Set<Integer> setMmsi = new LinkedHashSet<>();
		for (final PositionHistory positionHistory : list) {
			final PlaybackItem item = new PlaybackItem();
			final int id = positionHistory.getMmsi();
			setMmsi.add(id);
			item.setMmsi(id);
			item.setPoint(positionHistory.getPoint());
			final Date date = positionHistory.getTimestamp();
			item.setTime(date.getTime());
			item.setTimestamp(date);
			items.add(item);
		}
		result.setItems(items);
		Map<Integer, String> map = vesselService.findShipName(new ArrayList<>(
				setMmsi));
		result.setShip(map);
		return result;
	}
}
