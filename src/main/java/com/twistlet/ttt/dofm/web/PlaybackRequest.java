package com.twistlet.ttt.dofm.web;

import java.util.Date;
import java.util.List;

public class PlaybackRequest {

	private List<Integer> mmsi;
	private Date from;
	private Date to;

	public List<Integer> getMmsi() {
		return mmsi;
	}

	public void setMmsi(final List<Integer> mmsi) {
		this.mmsi = mmsi;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(final Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(final Date to) {
		this.to = to;
	}

}
