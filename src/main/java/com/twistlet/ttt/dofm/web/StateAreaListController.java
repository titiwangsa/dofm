package com.twistlet.ttt.dofm.web;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twistlet.ttt.dofm.model.entity.State;
import com.twistlet.ttt.dofm.model.service.StateManagementService;

@Controller
public class StateAreaListController {

	private StateManagementService stateManagementService;

	@Autowired
	public StateAreaListController(StateManagementService stateManagementService) {
		super();
		this.stateManagementService = stateManagementService;
	}

	@ResponseBody
	@RequestMapping("/list/list-area")
	public Collection<String> listArea(@RequestParam("key") String stateId) {
		State state = stateManagementService.get(stateId);
		if (state != null) {
			return state.getRegions();
		} else {
			return Collections.emptyList();
		}
	}

}
