package com.twistlet.ttt.dofm.web;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twistlet.ttt.dofm.model.entity.User;
import com.twistlet.ttt.dofm.model.service.UserManagementService;

@Controller
public class OwnerIdListController {

	private UserManagementService userManagementService;

	@Autowired
	public OwnerIdListController(UserManagementService userManagementService) {
		this.userManagementService = userManagementService;
	}

	@ResponseBody
	@RequestMapping("/list/list-owner")
	public Collection<String> listOwner() {
		List<User> listOfUser = userManagementService.listOwner();
		Set<String> set = new LinkedHashSet<String>();
		for (User user : listOfUser) {
			set.add(user.getUsername());
		}
		return set;
	}
}
