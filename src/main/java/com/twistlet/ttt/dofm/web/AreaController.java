package com.twistlet.ttt.dofm.web;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.twistlet.ttt.dofm.model.entity.State;
import com.twistlet.ttt.dofm.model.service.AreaService;
import com.twistlet.ttt.dofm.model.service.StateManagementService;

@Controller
public class AreaController {

	private final StateManagementService stateManagementService;
	private final AreaService areaService;

	@Autowired
	public AreaController(final StateManagementService stateManagementService,
			final AreaService areaService) {
		this.stateManagementService = stateManagementService;
		this.areaService = areaService;
	}

	@RequestMapping("/admin/area/list")
	public ModelAndView list(@RequestParam("name") final String name) {
		final State state = stateManagementService.get(name);
		final ModelAndView mav = new ModelAndView();
		mav.addObject("state", state);
		return mav;
	}

	@ResponseBody
	@RequestMapping("/admin/area/set")
	public Set<String> set(@RequestParam("name") final String name,
			@RequestParam("areas") final Set<String> areas) {
		areaService.set(name, areas);
		return areaService.listAreas(name);
	}
}
