package com.twistlet.ttt.dofm.web;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import com.twistlet.ttt.dofm.model.entity.LocalShip;
import com.twistlet.ttt.dofm.model.entity.State;
import com.twistlet.ttt.dofm.model.service.LocalShipManagementService;
import com.twistlet.ttt.dofm.model.service.StateManagementService;
import com.twistlet.ttt.dofm.model.service.UserManagementService;

@Controller
public class LocalShipManagementController {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private final LocalShipManagementService localShipManagementService;
	private final StateManagementService stateManagementService;
	private UserManagementService userManagementService;

	@Autowired
	public LocalShipManagementController(
			final LocalShipManagementService localShipManagementService,
			StateManagementService stateManagementService,
			UserManagementService userManagementService) {
		this.localShipManagementService = localShipManagementService;
		this.stateManagementService = stateManagementService;
		this.userManagementService = userManagementService;
	}

	@RequestMapping("/admin/localship/list")
	public ModelAndView list() {
		final ModelAndView mav = new ModelAndView();
		final List<LocalShip> list = localShipManagementService.list();
		mav.addObject("list", list);
		return mav;
	}

	@RequestMapping("/admin/localship/add")
	public ModelAndView index(
			@RequestParam(value = "error", required = false) final String error) {
		final ModelAndView mav = new ModelAndView();
		List<State> listState = stateManagementService.list();
		mav.addObject("listState", listState);
		mav.addObject("listOwner", userManagementService.listOwner());
		if (error != null) {
			mav.addObject("error", error);
		}
		return mav;
	}

	@RequestMapping("/admin/localship/save")
	public ModelAndView save(final LocalShipForm form) {
		final String viewError = "redirect:/admin/localship/add?error=Unable to save";
		if (form.isValid()) {
			try {
				final LocalShip localShip = new LocalShip();
				localShip.setMmsi(form.getMmsi());
				State state = stateManagementService.get(form.getState());
				localShip.setState(state.getName());
				localShip.setArea(form.getArea());
				localShip.setOwnerId(form.getOwnerId());
				localShipManagementService.save(localShip);
				return new ModelAndView("redirect:/admin/localship/list");
			} catch (final Exception e) {
				logger.error(e.toString());
			}
		}
		return new ModelAndView(viewError);
	}

	@ResponseBody
	@RequestMapping(value = "/admin/localship/remove", method = RequestMethod.POST)
	public Map<String, String> removeLocalShip(
			@RequestParam("id") final Integer id) {
		final Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("id", id.toString());
		try {
			localShipManagementService.remove(id);
			map.put("code", "0");
		} catch (final Exception e) {
			logger.error(e.toString());
			map.put("code", "1");
			map.put("message", e.toString());
		}
		return map;
	}

	@RequestMapping("/admin/localship/edit")
	public ModelAndView edit(@RequestParam(value = "id") final Integer id)
			throws Exception {
		final ModelAndView mav = new ModelAndView();
		final LocalShipForm form = new LocalShipForm();
		mav.addObject("form", form);
		List<State> listState = stateManagementService.list();
		mav.addObject("listState", listState);
		mav.addObject("listOwner", userManagementService.listOwner());
		final LocalShip localShip = localShipManagementService.get(id);
		if (localShip != null) {
			form.setMmsi(localShip.getMmsi());
			form.setState(localShip.getState());
			form.setArea(localShip.getArea());
			form.setOwnerId(localShip.getOwnerId());
			mav.addObject("listArea", listArea(listState, localShip.getState()));
			return mav;
		} else {
			throw new NoSuchRequestHandlingMethodException("edit", getClass());
		}
	}

	private Set<String> listArea(List<State> list, String state) {
		for (State item : list) {
			if (StringUtils.equals(item.getName(), state)) {
				return item.getRegions();
			}
		}
		return Collections.emptySet();
	}

	@RequestMapping("/admin/localship/update")
	public ModelAndView update(final LocalShipForm form) throws Exception {
		final ModelAndView mav = new ModelAndView();
		try {
			LocalShip localShip = new LocalShip();
			localShip.setMmsi(form.getMmsi());
			localShip.setState(form.getState());
			localShip.setArea(form.getArea());
			localShip.setOwnerId(form.getOwnerId());
			localShipManagementService.save(localShip);
			mav.setViewName("redirect:/admin/localship/list");
			return mav;
		} catch (final Exception e) {
			logger.error(e.toString());
			throw new NoSuchRequestHandlingMethodException("update", getClass());
		}
	}

}
