package com.twistlet.ttt.dofm.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twistlet.ttt.dofm.model.service.ActivePosition;
import com.twistlet.ttt.dofm.model.service.MapService;

@Controller
public class CoordinateController {

	private final MapService mapService;

	@Autowired
	public CoordinateController(final MapService mapService) {
		super();
		this.mapService = mapService;
	}

	@RequestMapping("/coordinates")
	@ResponseBody
	public CoordinateResult get() {
		final List<ActivePosition> items = mapService.listVessel();
		final CoordinateResult result = new CoordinateResult();
		result.setItems(items);
		result.setTimestamp(System.currentTimeMillis());
		return result;
	}
}
