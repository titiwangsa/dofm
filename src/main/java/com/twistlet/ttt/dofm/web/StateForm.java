package com.twistlet.ttt.dofm.web;

import org.apache.commons.lang3.StringUtils;

public class StateForm {

	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public boolean isValid() {
		if (StringUtils.isBlank(name)) {
			return false;
		}
		return true;
	}

}
