package com.twistlet.ttt.dofm.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twistlet.ttt.dofm.model.service.MapService;

@Controller
public class ShorelineController {

	private final MapService mapService;

	@Autowired
	public ShorelineController(final MapService mapService) {
		super();
		this.mapService = mapService;
	}

	@ResponseBody
	@RequestMapping("/shoreline/{groupId}")
	public List<Point> get(@PathVariable("groupId") final Integer groupId) {
		return mapService.listShoreline(groupId);
	}

	@ResponseBody
	@RequestMapping("/shorelines")
	public List<List<Point>> get() {
		return mapService.listShorelines();
	}
}
