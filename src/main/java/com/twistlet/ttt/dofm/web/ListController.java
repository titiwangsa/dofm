package com.twistlet.ttt.dofm.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twistlet.ttt.dofm.model.entity.Vessel;
import com.twistlet.ttt.dofm.model.service.IdPositionVessel;
import com.twistlet.ttt.dofm.model.service.ListService;

@Controller
public class ListController {

	private final ListService listService;

	@Autowired
	public ListController(final ListService listService) {
		this.listService = listService;
	}

	@ResponseBody
	@RequestMapping("/list-ship-name")
	public List<IdNamePair> listShipName() {
		final List<IdNamePair> list = new ArrayList<>();
		final List<IdPositionVessel> items = listService.listPositionVessel();
		for (final IdPositionVessel item : items) {
			final Vessel vessel = item.getVessel();
			final Integer id = item.getId();
			final String itemId = id.toString();
			final IdNamePair pair = new IdNamePair(itemId, vessel.getName());
			list.add(pair);
		}
		Collections.sort(list, new NameComparator());
		return list;
	}

	private class NameComparator implements Comparator<IdNamePair> {

		@Override
		public int compare(final IdNamePair o1, final IdNamePair o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}

	public class IdNamePair {
		private final String id;
		private final String name;

		public IdNamePair(final String id, final String name) {
			this.id = id;
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

	}
}
