package com.twistlet.ttt.dofm.web;

import org.apache.commons.lang3.StringUtils;

public class LocalShipForm {

	private Integer mmsi;
	private String state;
	private String area;
	private String ownerId;

	public Integer getMmsi() {
		return mmsi;
	}

	public void setMmsi(Integer mmsi) {
		this.mmsi = mmsi;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public boolean isValid() {
		if (StringUtils.isBlank(state)) {
			return false;
		}
		return true;
	}

}
