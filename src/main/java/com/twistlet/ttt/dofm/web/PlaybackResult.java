package com.twistlet.ttt.dofm.web;

import java.util.List;
import java.util.Map;

public class PlaybackResult {

	private Map<Integer, String> ship;
	private List<PlaybackItem> items;

	public Map<Integer, String> getShip() {
		return ship;
	}

	public void setShip(final Map<Integer, String> ship) {
		this.ship = ship;
	}

	public List<PlaybackItem> getItems() {
		return items;
	}

	public void setItems(final List<PlaybackItem> items) {
		this.items = items;
	}

}
