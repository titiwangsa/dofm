package com.twistlet.ttt.dofm.web;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class PlaybackItem {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy hh:mm:ss a", timezone = "GMT+8")
	private Date timestamp;

	private long time;

	private int mmsi;

	private double[] point;

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(final Date timestamp) {
		this.timestamp = timestamp;
	}

	public long getTime() {
		return time;
	}

	public void setTime(final long time) {
		this.time = time;
	}

	public int getMmsi() {
		return mmsi;
	}

	public void setMmsi(final int mmsi) {
		this.mmsi = mmsi;
	}

	public double[] getPoint() {
		return point;
	}

	public void setPoint(final double[] point) {
		this.point = point;
	}

}
