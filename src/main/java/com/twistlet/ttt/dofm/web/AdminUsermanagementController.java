package com.twistlet.ttt.dofm.web;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import com.twistlet.ttt.dofm.model.entity.User;
import com.twistlet.ttt.dofm.model.service.UserManagementService;

@Controller
public class AdminUsermanagementController {

	private final UserManagementService userManagementService;
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	public AdminUsermanagementController(
			final UserManagementService userManagementService) {
		this.userManagementService = userManagementService;
	}

	@RequestMapping("/admin/usermanagement/list")
	public ModelAndView list() {
		final ModelAndView mav = new ModelAndView();
		final List<User> list = userManagementService.list();
		mav.addObject("list", list);
		return mav;
	}

	@RequestMapping("/admin/usermanagement/add")
	public ModelAndView index(
			@RequestParam(value = "error", required = false) final String error) {
		final ModelAndView mav = new ModelAndView();
		if (error != null) {
			mav.addObject("error", error);
		}
		return mav;
	}

	@RequestMapping("/admin/usermanagement/edit")
	public ModelAndView edit(@RequestParam(value = "id") final String id)
			throws Exception {
		final ModelAndView mav = new ModelAndView();
		final UserUpdateForm form = new UserUpdateForm();
		mav.addObject("form", form);
		final User user = userManagementService.get(id);
		if (user != null) {
			form.setUsername(user.getUsername());
			form.setFullName(user.getFullName());
			form.setRole(user.getRole());
			form.setEnabled(user.getEnabled());
			return mav;
		} else {
			throw new NoSuchRequestHandlingMethodException("edit",
					AdminUsermanagementController.class);
		}
	}

	@RequestMapping("/admin/usermanagement/save")
	public ModelAndView save(final UserRegistrationForm form) {
		final String viewError = "redirect:/admin/usermanagement/add?error=Unable to save";
		if (form.isValid()
				&& (null == userManagementService.get(form.getUsername()))) {
			try {
				userManagementService.save(form.getUsername(), form.getName(),
						form.getPassword(), form.getRole());
				return new ModelAndView("redirect:/admin/usermanagement/list");
			} catch (final Exception e) {
				logger.error(e.toString());
			}
		}
		return new ModelAndView(viewError);
	}

	@RequestMapping("/admin/usermanagement/update")
	public ModelAndView update(final UserUpdateForm form) throws Exception {
		final ModelAndView mav = new ModelAndView();
		try {
			userManagementService.update(form.getUsername(),
					form.getFullName(), form.getRole(), form.getEnabled());
			mav.setViewName("redirect:/admin/usermanagement/list");
			return mav;
		} catch (final Exception e) {
			logger.error(e.toString());
			throw new NoSuchRequestHandlingMethodException("update", getClass());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/admin/usermanagement/remove", method = RequestMethod.POST)
	public Map<String, String> remove(@RequestParam("id") final String id) {
		final Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("username", id);
		try {
			userManagementService.remove(id);
			map.put("code", "0");
		} catch (final Exception e) {
			logger.error(e.toString());
			map.put("code", "1");
			map.put("message", e.toString());
		}
		return map;
	}

	@ResponseBody
	@RequestMapping(value = "/admin/usermanagement/change-password", method = RequestMethod.POST)
	public Map<String, String> changePassword(final UserRegistrationForm form) {
		final Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("username", form.getUsername());
		try {
			userManagementService.changePassword(form.getUsername(),
					form.getPassword());
			map.put("code", "0");
		} catch (final Exception e) {
			logger.error(e.toString());
			map.put("code", "1");
			map.put("message", e.toString());
		}
		return map;
	}

}
