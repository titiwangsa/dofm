package com.twistlet.ttt.dofm.web;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.twistlet.ttt.dofm.model.entity.State;
import com.twistlet.ttt.dofm.model.service.StateManagementService;

@Controller
public class StateManagementController {

	private final StateManagementService stateManagementService;
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	public StateManagementController(
			final StateManagementService stateManagementService) {
		this.stateManagementService = stateManagementService;
	}

	@RequestMapping("/admin/state/list")
	public ModelAndView list() {
		final ModelAndView mav = new ModelAndView();
		final List<State> list = stateManagementService.list();
		mav.addObject("list", list);
		return mav;
	}

	@RequestMapping("/admin/state/add")
	public ModelAndView index(
			@RequestParam(value = "error", required = false) final String error) {
		final ModelAndView mav = new ModelAndView();
		if (error != null) {
			mav.addObject("error", error);
		}
		return mav;
	}

	@RequestMapping("/admin/state/save")
	public ModelAndView save(final StateForm form) {
		final String viewError = "redirect:/admin/state/add?error=Unable to save";
		if (form.isValid()) {
			try {
				final State state = new State();
				state.setName(form.getName());
				state.setRegions(new LinkedHashSet<String>());
				stateManagementService.save(state);
				return new ModelAndView("redirect:/admin/state/list");
			} catch (final Exception e) {
				logger.error(e.toString());
			}
		}
		return new ModelAndView(viewError);
	}

	@ResponseBody
	@RequestMapping(value = "/admin/state/remove", method = RequestMethod.POST)
	public Map<String, String> removeState(@RequestParam("id") final String id) {
		final Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("id", id);
		try {
			stateManagementService.remove(id);
			map.put("code", "0");
		} catch (final Exception e) {
			logger.error(e.toString());
			map.put("code", "1");
			map.put("message", e.toString());
		}
		return map;
	}

}
