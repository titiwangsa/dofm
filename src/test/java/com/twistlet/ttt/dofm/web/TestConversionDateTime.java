package com.twistlet.ttt.dofm.web;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.time.FastDateFormat;
import org.joda.time.DateTime;
import org.junit.Test;

public class TestConversionDateTime {

	@Test
	public void testTimeZone() {
		final TimeZone tz = TimeZone.getDefault();
		final int offset = tz.getRawOffset();
		assertEquals(1000 * 60 * 60 * 8, offset);
	}

	@Test
	public void testPlaybackDateTimeConversionDateToString() {
		final FastDateFormat sdf = FastDateFormat
				.getInstance("dd/MM/yyyy hh:mm:ss aa");
		final Date date = new DateTime(2012, 5, 10, 5, 8, 9).toDate();
		final String actual = sdf.format(date);
		assertEquals("10/05/2012 05:08:09 AM", actual);
	}

	@Test
	public void testPlaybackDateTimeConversionDateFromString()
			throws ParseException {
		final String text = "11/07/2012 05:08:09 PM";
		final SimpleDateFormat sdf = new SimpleDateFormat(
				"dd/MM/yyyy hh:mm:ss aa");
		final Date actual = sdf.parse(text);
		assertTrue(actual.toString().matches("Wed Jul 11 17:08:09 ... 2012"));
	}
}
