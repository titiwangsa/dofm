package com.twistlet.ttt.dofm.model.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AisServiceImpTest {

	@Test
	public void test1() {
		final AisServiceImp unit = new AisServiceImp();
		final int messageType = unit.getType("17tkFd501N7=Q0h22VHDM3Sl08Qr");
		assertEquals(1, messageType);
	}

}
