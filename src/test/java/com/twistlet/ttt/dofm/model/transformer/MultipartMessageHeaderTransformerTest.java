package com.twistlet.ttt.dofm.model.transformer;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.integration.IntegrationMessageHeaderAccessor;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

public class MultipartMessageHeaderTransformerTest {

	@Test
	public void testTransform() {
		final String line = "!AIVDM,2,1,7,B,55?KUj02;qP;<H9K:21@58tdv222222222222216:`@8;4pC0;C5@UBp1hEC,0*43";
		final MultipartMessageHeaderTransformer unit = new MultipartMessageHeaderTransformer();
		final MessageBuilder<String> messageBuilder = MessageBuilder
				.withPayload(line);
		final Message<?> newMessage = unit.transform(messageBuilder.build());
		final IntegrationMessageHeaderAccessor accessor = new IntegrationMessageHeaderAccessor(
				newMessage);
		final Integer actual = accessor.getSequenceNumber();
		final Integer expected = new Integer(1);
		assertEquals(expected, actual);
	}
}
