package com.twistlet.ttt.dofm.model.repository;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.twistlet.ttt.dofm.model.entity.Shoreline;

@ContextConfiguration({ "classpath:spring-context-config.xml",
		"classpath:test-spring-context-config-properties.xml",
		"classpath:spring-context-mongo.xml" })
public class ShorelineRepositoryImplIT extends AbstractJUnit4SpringContextTests {

	@Autowired
	MongoOperations mongoOperations;

	@Autowired
	ShorelineRepository shorelineRepository;

	@Before
	public void init() {
	}

	@Test
	public void testFindGeoResultsByPointNear() {
		{
			final Shoreline shoreline = new Shoreline();
			shoreline.setGroupId(1);
			shoreline.setSequenceId(1);
			shoreline.setPoint(new double[] { -74.044628, 40.689182 });
			shorelineRepository.save(shoreline);
		}
		{
			final Shoreline shoreline = new Shoreline();
			shoreline.setGroupId(1);
			shoreline.setSequenceId(2);
			shoreline.setPoint(new double[] { -74.041243, 40.700309 });
			shorelineRepository.save(shoreline);
		}

		final GeoResults<Shoreline> results = shorelineRepository
				.findGeoResultsByPointNear(-74.045255, 40.702554);
		final List<GeoResult<Shoreline>> items = results.getContent();
		final GeoResult<Shoreline> item = items.get(0);
		assertThat(item.getDistance().getValue(),
				closeTo(0.22722592696570906, 0.00001));
		assertThat(item.getContent(), notNullValue());
		assertThat(item.getContent().getPoint(), notNullValue());
		assertThat(item.getContent().getPoint()[0], equalTo(-74.041243));
		assertThat(item.getContent().getPoint()[1], equalTo(40.700309));
	}
}
