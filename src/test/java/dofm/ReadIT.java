package dofm;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration({ "classpath:spring-context-config.xml",
		"classpath:spring-context-config-properties.xml",
		"classpath:spring-context-integration-input.xml",
		"classpath:test-read-it.xml" })
public class ReadIT extends AbstractJUnit4SpringContextTests {

	@Test
	public void test() throws InterruptedException {
		Thread.sleep(5_000);
	}
}
