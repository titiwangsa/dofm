package populate;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.twistlet.ttt.dofm.model.entity.Geofence;
import com.twistlet.ttt.dofm.model.entity.Shoreline;
import com.twistlet.ttt.dofm.model.repository.GeofenceRepository;
import com.twistlet.ttt.dofm.model.repository.ShorelineRepository;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.LineString;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

public class PopulateMalaysiaMap {
	private ShorelineRepository shorelineRepository;
	private GeofenceRepository geofenceRepository;

	public static void main(final String[] args) {
		new PopulateMalaysiaMap().run();
	}

	private void run() {
		try (GenericXmlApplicationContext applicationContext = new GenericXmlApplicationContext()) {

			final Resource[] resources = {
					new ClassPathResource("spring-context-mongo.xml"),
					new ClassPathResource(
							"spring-context-config-properties.xml"),
					new ClassPathResource("spring-context-config.xml") };
			applicationContext.load(resources);
			applicationContext.refresh();
			shorelineRepository = applicationContext
					.getBean(ShorelineRepository.class);
			geofenceRepository = applicationContext
					.getBean(GeofenceRepository.class);
			populateShoreline(new ClassPathResource("kml/shoreline.kml.xml")
					.getFile());
			populateGeoFence(new ClassPathResource("kml/geofence_5nm.kml.xml")
					.getFile());
		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	private void populateGeoFence(final File file) {
		final Kml kml = Kml.unmarshal(file);
		final Document document = (Document) kml.getFeature();
		final List<Feature> list = document.getFeature();
		geofenceRepository.deleteAll();
		for (final Feature feature : list) {
			if (feature instanceof Placemark) {
				final Placemark placemark = (Placemark) feature;
				final LineString lineString = (LineString) placemark
						.getGeometry();
				final List<Coordinate> coordinates = lineString
						.getCoordinates();
				final Geofence geofence = new Geofence();
				final List<double[]> points = new ArrayList<>();
				for (final Coordinate coordinate : coordinates) {
					final double[] point = new double[] {
							coordinate.getLongitude(), coordinate.getLatitude() };
					points.add(point);
				}
				geofence.setPoints(points);
				geofence.setLabel("5 nm");
				geofenceRepository.save(geofence);
			}
		}
		System.out.println("Geofence 5nm stored");

	}

	private void populateShoreline(final File file) {
		final Kml kml = Kml.unmarshal(file);
		final Document document = (Document) kml.getFeature();
		final List<Feature> list = document.getFeature();
		int groupId = 0;
		shorelineRepository.deleteAll();
		int total = 0;
		for (final Feature feature : list) {
			if (feature instanceof Placemark) {
				final Placemark placemark = (Placemark) feature;
				final LineString lineString = (LineString) placemark
						.getGeometry();
				final List<Coordinate> coordinates = lineString
						.getCoordinates();
				if (coordinates.size() == 0) {
					continue;
				}
				int index = 0;
				for (final Coordinate coordinate : coordinates) {
					final Shoreline shoreline = new Shoreline();
					shoreline.setGroupId(groupId);
					shoreline.setSequenceId(index);
					shoreline
							.setPoint(new double[] { coordinate.getLongitude(),
									coordinate.getLatitude() });
					shorelineRepository.save(shoreline);
					total++;
					index++;
				}
				System.out.println(" group " + groupId + ", " + index
						+ " items inserted");
				groupId++;
			}
		}
		System.out.println(total + " shorelines inserted");
	}
}
